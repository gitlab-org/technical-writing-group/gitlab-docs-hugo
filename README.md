# GitLab Docs

This project generates the GitLab documentation website.

For information on:

- The build and deployment process, see [doc/index.md](doc/index.md).
- Editing GitLab documentation, see [GitLab Documentation guidelines](https://docs.gitlab.com/development/documentation/).

See also:

- [LICENSE](LICENSE): MIT License.

/* eslint-disable import/no-default-export */
import path from "node:path";
import { fileURLToPath } from "node:url";
import { FlatCompat } from "@eslint/eslintrc";
import js from "@eslint/js";
import jest from "eslint-plugin-jest";
import pluginVue from 'eslint-plugin-vue';

const filename = fileURLToPath(import.meta.url);
const dirname = path.dirname(filename);
const compat = new FlatCompat({
  baseDirectory: dirname,
  recommendedConfig: js.configs.recommended,
  allConfig: js.configs.all
});

export default [
  ...pluginVue.configs['flat/vue2-recommended'],
  {
    ignores: [
      "**/vite.config.js",
      "**/postcss.config.js",
      "coverage/",
      "node_modules/",
      "public/",
      "tmp/",
      "themes/gitlab-docs/static/vite",
    ],
  },
  ...compat.extends("plugin:@gitlab/default"),
  {
    plugins: {
      jest,
    },
    languageOptions: {
      globals: {
        ...jest.environments.globals.globals,
      },
    },
    rules: {
      "no-console": ["error", {
        allow: ["info", "warn", "error"],
      }],
      "no-param-reassign": ["error", {
        props: false,
      }],
      "allowForLoopAfterthoughts":"off",
      "max-params": "off",
    },
  }
];

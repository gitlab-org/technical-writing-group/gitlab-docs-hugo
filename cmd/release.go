package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/cmd/gldocs/tasks"
)

var releaseCmd = &cobra.Command{
	Use:   "release",
	Short: "Create stable branch and Docker image for a new release.",
	Long:  `Create stable branch and Docker image for a new release. See https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/.gitlab/issue_templates/release.md?ref_type=heads#create-a-stable-branch-and-docker-image-for-the-release for more information. `,
	Run:   createReleaseBranch,
}

var releaseMRCmd = &cobra.Command{
	Use:   "releaseMR",
	Short: "Create merge request to update the version dropdown list for all online versions, update the archives list, and add the release to the Docker configuration.",
	Long:  `Create merge request to update the version dropdown list for all online versions, update the archives list, and add the release to the Docker configuration. https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/.gitlab/issue_templates/release.md?ref_type=heads#on-the-thursday-of-the-release-or-the-day-after for more information. `,
	Run:   createReleaseMR,
}

func init() {
	rootCmd.AddCommand(releaseCmd)
	rootCmd.AddCommand(releaseMRCmd)
}

func createReleaseBranch(cmd *cobra.Command, args []string) {
	if len(args) == 0 {
		log.Fatal("Usage: VERSION argument missing!")
	} else {
		tasks.CreateReleaseArchive(args[0])
	}
}

func createReleaseMR(cmd *cobra.Command, args []string) {
	// Check if GITLAB_TOKEN is set as an environment variable.
	val, exists := os.LookupEnv("GITLAB_TOKEN")
	if val == "" || !exists {
		log.Fatal("Usage: GITLAB_TOKEN environment variable is missing!")
	}

	// Check if version is missing
	if len(args) == 0 {
		log.Fatal("Usage: VERSION argument missing!")
	}

	// If all checks pass, continue to create MR with changes.
	tasks.CreateMergeRequest(args[0])
}

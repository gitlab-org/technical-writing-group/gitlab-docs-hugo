package tasks

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func mockRunGitCommand(args ...string) (string, error) {
	if len(args) == 3 && args[0] == "branch" && args[1] == "--list" {
		if args[2] == "main" {
			return "* main", nil
		}
		return "", nil
	}
	return "", fmt.Errorf("unexpected command")
}

func TestLocalBranchExists(t *testing.T) {
	testCases := []struct {
		name        string
		branch      string
		shouldExist bool
	}{
		{
			name:        "Branch exists case",
			branch:      "main",
			shouldExist: true,
		},
		{
			name:        "Branch does not exist case",
			branch:      "nonexistent-branch",
			shouldExist: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actualExists := localBranchExists(tc.branch, mockRunGitCommand)
			require.Equal(t, tc.shouldExist, actualExists)
		})
	}
}

func TestFileExists(t *testing.T) {
	testCases := []struct {
		name        string
		fileName    string
		shouldExist bool
	}{
		{
			name:        "Filename exists case",
			fileName:    "../../../Makefile",
			shouldExist: true,
		},
		{
			name:        "Filename does not exist case",
			fileName:    "fakeFile.txt",
			shouldExist: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actualExists := fileExists(tc.fileName)
			require.Equal(t, tc.shouldExist, actualExists)
		})
	}
}

func TestAskToOverwrite(t *testing.T) {
	// Create a temporary file
	tempDir := t.TempDir()
	tempFile := filepath.Join(tempDir, "test_file.txt")
	err := os.WriteFile(tempFile, []byte("test data"), 0600)
	if err != nil {
		t.Fatalf("Failed to create temporary file: %v", err)
	}

	testCases := []struct {
		name      string
		input     string
		overwrite bool
	}{
		{
			name:      "lowercase input - y",
			input:     "y",
			overwrite: true,
		},
		{
			name:      "uppercase input - y",
			input:     "Y",
			overwrite: true,
		},
		{
			name:      "uppercase input - N",
			input:     "N",
			overwrite: false,
		},
		{
			name:      "lowercase input - n",
			input:     "n",
			overwrite: false,
		},
		{
			name:      "invalid input - invalid",
			input:     "invalid",
			overwrite: false,
		},
	}

	for _, tc := range testCases {
		// Simulate user input
		oldStdin := os.Stdin
		defer func() { os.Stdin = oldStdin }()

		r, w, _ := os.Pipe()
		os.Stdin = r

		errChan := make(chan error, 1)
		go func() {
			_, err := io.WriteString(w, tc.input)
			if err != nil {
				errChan <- err
			}
			w.Close()
			close(errChan)
		}()

		if err := <-errChan; err != nil {
			t.Fatalf("%v", err)
		}

		// Capture stdout
		oldStdout := os.Stdout
		defer func() { os.Stdout = oldStdout }()
		os.Stdout = oldStdout

		t.Run(tc.name, func(t *testing.T) {
			actualAnswer := askToOverwrite(tempFile)
			require.Equal(t, tc.overwrite, actualAnswer)
		})
	}
}

func TestNextVersion(t *testing.T) {
	testCases := []struct {
		name           string
		currentVersion string
		expected       string
		err            error
	}{
		{
			name:           "Current version's minor version is a single digit. Increment by 0.1.",
			currentVersion: "17.2",
			expected:       "17.3",
		},
		{
			name:           "Current version's minor version is 11. Increment current's major version by 1 and set minor version to 0.",
			currentVersion: "17.11",
			expected:       "18.0",
		},
		{
			name:           "Current version's minor version is a double digit but < 11. Increment current's minor version by 0.01.",
			currentVersion: "17.10",
			expected:       "17.11",
		},
		{
			name:           "Bad version provided. Error expected",
			currentVersion: "bad",
			expected:       "",
			err:            fmt.Errorf("Failed to parse current version."),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actual, err := nextVersion(tc.currentVersion)
			require.Equal(t, tc.expected, actual)
			require.Equal(t, tc.err, err)
		})
	}
}

func TestGetLastMajorVersions(t *testing.T) {
	testCases := []struct {
		name                    string
		inputCurentVer          string
		inputLastMajorVer       string
		inputSecondLastMajorVer string
		expected                []string
	}{
		{
			name:                    "Difference between current version's major version (17) AND last major version's (16) is 1. Don't update major versions.",
			inputCurentVer:          "17.2",
			inputLastMajorVer:       "16.11",
			inputSecondLastMajorVer: "15.11",
			expected:                []string{"16.11", "15.11"},
		},
		{
			name:                    "Difference between current version's major version (17) AND last major version's (15) is 2 (more than one major version difference). Update major versions.",
			inputCurentVer:          "17.0",
			inputLastMajorVer:       "15.11",
			inputSecondLastMajorVer: "14.11",
			expected:                []string{"16.11", "15.11"},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actual := getLastMajorVersions(tc.inputCurentVer, tc.inputLastMajorVer, tc.inputSecondLastMajorVer)
			require.Equal(t, tc.expected, actual)
		})
	}
}

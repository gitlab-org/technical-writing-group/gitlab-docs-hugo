package tasks

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestBuildFeatureFlagsData(t *testing.T) {
	// Create a temporary directory for test files
	tempDir, err := os.MkdirTemp("", "test")
	require.NoError(t, err)
	defer os.RemoveAll(tempDir)

	testCases := []struct {
		name     string
		paths    map[string]string
		expected map[string]interface{}
		setup    func(string) error
	}{
		{
			name: "Valid YAML files",
			paths: map[string]string{
				"product1": filepath.Join(tempDir, "product1_*.yaml"),
				"product2": filepath.Join(tempDir, "product2_*.yaml"),
			},
			expected: map[string]interface{}{
				"products": map[string][]map[string]string{
					"product1": {{"key1": "value1"}, {"key2": "value2"}},
					"product2": {{"key3": "value3"}},
				},
			},
			setup: func(dir string) error {
				if err := os.WriteFile(filepath.Join(dir, "product1_1.yaml"), []byte("key1: value1"), 0400); err != nil {
					return err
				}
				if err := os.WriteFile(filepath.Join(dir, "product1_2.yaml"), []byte("key2: value2"), 0400); err != nil {
					return err
				}
				if err := os.WriteFile(filepath.Join(dir, "product2_1.yaml"), []byte("key3: value3"), 0400); err != nil {
					return err
				}
				return nil
			},
		},
		{
			name: "Invalid file path",
			paths: map[string]string{
				"product1": filepath.Join(tempDir, "badpath", "*.yaml"),
			},
			expected: map[string]interface{}{
				"products": map[string][]map[string]string{},
			},
			setup: func(dir string) error { return nil },
		},
		{
			name: "Invalid YAML content",
			paths: map[string]string{
				"product1": filepath.Join(tempDir, "invalid_*.yaml"),
			},
			expected: nil,
			setup: func(dir string) error {
				return os.WriteFile(filepath.Join(dir, "invalid_1.yaml"), []byte("invalid: yaml: content"), 0400)
			},
		},
		{
			name:     "Empty paths",
			paths:    map[string]string{},
			expected: map[string]interface{}{"products": map[string][]map[string]string{}},
			setup:    func(dir string) error { return nil },
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Setup test files
			require.NoError(t, tc.setup(tempDir))

			// Run the function
			result, err := buildFeatureFlagsData(tc.paths)

			if tc.expected == nil {
				require.Error(t, err)
				require.Nil(t, result)
			} else {
				require.NoError(t, err)
				require.Equal(t, tc.expected, result)
			}
		})
	}
}

func TestBuildFeatureFlags(t *testing.T) {
	// Setup
	tempDir, err := os.MkdirTemp("", "test")
	require.NoError(t, err)
	defer os.RemoveAll(tempDir)

	// Store original working directory and change to temp directory
	originalWD, err := os.Getwd()
	require.NoError(t, err)
	err = os.Chdir(tempDir)
	require.NoError(t, err)
	defer func() {
		err := os.Chdir(originalWD)
		require.NoError(t, err)
	}()

	// Create mock directory structure
	err = os.MkdirAll(filepath.Join("gitlab", "config", "feature_flags"), 0700)
	require.NoError(t, err)
	err = os.MkdirAll(filepath.Join("gitlab", "ee", "config", "feature_flags"), 0700)
	require.NoError(t, err)

	// Create some mock feature flag files
	mockFeatureFlag := []byte("name: test_flag\ntype: development\n")
	err = os.WriteFile(filepath.Join("gitlab", "config", "feature_flags", "test_flag.yml"), mockFeatureFlag, 0600)
	require.NoError(t, err)

	testCases := []struct {
		name      string
		setupFunc func()
		wantErr   bool
	}{
		{
			name:      "Successfully build feature flag YAML file",
			setupFunc: func() {},
			wantErr:   false,
		},
		{
			name: "Missing feature flags directory",
			setupFunc: func() {
				// Remove both directories to ensure no feature flags are found
				os.RemoveAll(filepath.Join("gitlab", "config", "feature_flags"))
				os.RemoveAll(filepath.Join("gitlab", "ee", "config", "feature_flags"))
			},
			wantErr: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			tc.setupFunc()

			err := BuildFeatureFlags()
			if tc.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
			}

			if !tc.wantErr {
				// Verify the output file exists in the expected location
				outputPath := filepath.Join("data", "feature_flags.yaml")
				_, err := os.Stat(outputPath)
				require.NoError(t, err, "Output file should exist")

				// Optionally verify file contents
				content, err := os.ReadFile(outputPath)
				require.NoError(t, err)
				require.NotEmpty(t, content)
			}
		})
	}
}

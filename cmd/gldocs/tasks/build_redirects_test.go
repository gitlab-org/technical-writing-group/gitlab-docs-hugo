package tasks

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestBuildRedirects(t *testing.T) {
	// Test successful case
	t.Run("successful redirect generation", func(t *testing.T) {
		testYAML := `redirects:
  - from: /old/path
    to: /new/path
    remove_date: "2024-01-01"
  - from: /another/old/*
    to: /another/new/:splat
  - from: /test
    to: /final`

		// Mock the file operations (if needed)
		// Test the redirect format generation
		var redirectsFile RedirectsFile
		require.NoError(t, yaml.Unmarshal([]byte(testYAML), &redirectsFile))

		expected := `/old/path /new/path 301
/another/old/* /another/new/:splat 301
/test /final 301
`
		var result strings.Builder
		for _, redirect := range redirectsFile.Redirects {
			result.WriteString(fmt.Sprintf("%s %s 301\n", redirect.From, redirect.To))
		}
		require.Equal(t, expected, result.String())
	})

	// Test error case - invalid YAML
	t.Run("invalid YAML content", func(t *testing.T) {
		invalidYAML := `redirects:
  - from: /old/path
    to: /new/path
    invalid yaml content
    - this is not valid yaml`

		var redirectsFile RedirectsFile
		err := yaml.Unmarshal([]byte(invalidYAML), &redirectsFile)
		require.Error(t, err)
	})
}

package tasks

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"gopkg.in/yaml.v3"
)

// A Product is a GitLab project that we include on the Docs site
type Product struct {
	Repo          string `yaml:"repo"`
	DefaultBranch string `yaml:"default_branch"`
	CloneDir      string `yaml:"clone_dir"`
}

type Products struct {
	Products map[string]Product `yaml:"products"`
}

func (p Product) StableBranchFor(productName string) string {
	stableBranchName := getStableBranchName()
	if stableBranchName == "" {
		return p.DefaultBranch
	}

	switch productName {
	case "gitlab":
		return fmt.Sprintf("%s-ee", stableBranchName)
	case "omnibus", "runner":
		return stableBranchName
	case "charts":
		return chartsStableBranch(stableBranchName)
	default:
		return p.DefaultBranch
	}
}

/**
 * Determine the Git branch and refspec to fetch.
 *
 * Use the BRANCH_* environment variable, and if not assigned,
 * set to the default branch.
 */
func (p Product) CloneInfo(productName string) (string, string) {
	/**
	 * The BRANCH_ and MERGE_REQUEST_IID_ variables that we use here come from
	 * https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/trigger-build.rb.
	 * Usually they match the product name, but the GitLab product uses "EE".
	 */
	envVarSuffix := func() string {
		if productName == "gitlab" {
			return "EE"
		}
		return strings.ToUpper(productName)
	}()

	mergeRequestIID := os.Getenv(fmt.Sprintf("MERGE_REQUEST_IID_%s", envVarSuffix))
	branchName := os.Getenv(fmt.Sprintf("BRANCH_%s", envVarSuffix))

	if branchName == "" {
		branchName = p.StableBranchFor(productName)
	}
	if mergeRequestIID == "" {
		return branchName, fmt.Sprintf("heads/%s", branchName)
	}

	return branchName, fmt.Sprintf("merge-requests/%s/head", mergeRequestIID)
}

func (p Product) Clone(productName string) error {
	cloneDir := p.CloneDir
	branch, refspec := p.CloneInfo(productName)

	// Remove the product repo if it already exists if REMOVE_BEFORE_CLONE is true,
	// or if we're in a CI environment.
	// This can happen if we land on a Runner that already ran a docs build.
	if _, err := os.Stat(cloneDir); err == nil && (os.Getenv("CI") == "true" || os.Getenv("REMOVE_BEFORE_CLONE") == "true") {
		err = os.RemoveAll(cloneDir)
		if err != nil {
			return fmt.Errorf("Error removing directory: %v\n", err)
		}

		log.Printf("[Info] %s already exists, removing it", cloneDir)
	}

	// If the directory exists, and it's a local environment, skip it.
	if _, err := os.Stat(cloneDir); err == nil && os.Getenv("CI") == "" {
		log.Printf("[Info] %s directory already exists, skipping", productName)

		return nil
	}

	// Create the target directory, and move into it
	if err := os.MkdirAll(cloneDir, os.ModePerm); err != nil {
		return fmt.Errorf("Error creating directory: %v\n", err)
	} else if err := os.Chdir(cloneDir); err != nil {
		return fmt.Errorf("Error changing directory: %v\n", err)
	}

	// Initialize the repository, and fetch the desired branch and refspec
	log.Printf("[Info] Fetching %s on branch %s at commit %s", productName, branch, refspec)
	_, err := runGitCommand("-c", fmt.Sprintf("init.defaultBranch=%s", branch), "init")
	if err != nil {
		return fmt.Errorf("Error initializing repository: %v\n", err)
	}
	_, err = runGitCommand("remote", "add", "origin", p.Repo)
	if err != nil {
		return fmt.Errorf("Error adding repository origin: %v\n", err)
	}
	_, err = runGitCommand("fetch", "--depth", "1", "origin", refspec)
	if err != nil {
		return fmt.Errorf("Error fetching repository log: %v\n", err)
	}
	_, err = runGitCommand("-c", "advice.detachedHead=false", "checkout", "FETCH_HEAD")
	if err != nil {
		return fmt.Errorf("Error checking out: %v\n", err)
	}

	// Print the last commit message
	logMessage, err := runGitCommand("log", "--oneline", "-n", "1")
	if err != nil {
		return fmt.Errorf("Error reading Git log: %v\n", err)
	}

	log.Printf("[Info] Last commit: %s", string(logMessage))

	return nil
}

func CloneProjects() {
	// Load product info
	productsData, err := readProductData("data/products.yaml")
	if err != nil {
		log.Fatalf("Error loading products.yaml: %v\n", err)
	}

	// Iterate through products
	for productName, product := range productsData.Products {
		err := product.Clone(productName)
		if err != nil {
			log.Fatal(err)
		}
	}
}

/**
 * Return product info from a given YAML file
 */
func readProductData(productsYaml string) (*Products, error) {
	data, err := os.ReadFile(productsYaml)
	if err != nil {
		return nil, err
	}
	var productsData Products
	err = yaml.Unmarshal(data, &productsData)

	return &productsData, err
}

func runGitCommand(args ...string) (string, error) {
	cmd := exec.Command("git", args...)
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Printf("Error running Git command 'git %v': %v\n", args, err)
		return "", err
	}

	return string(output), nil
}

/*
 * If we're on a gitlab-docs stable branch (or targeting one) according to the
 * regex, catch the version and return the branch name.
 * For example, 15-8-stable.
 *
 * 1. Skip if CI_COMMIT_REF_NAME is not defined (run outside the CI environment).
 * 2. If CI_COMMIT_REF_NAME matches the version format it means we're on a
 *    stable branch. Return the version format of that branch.
 * 3. If CI_MERGE_REQUEST_TARGET_BRANCH_NAME is defined and its value matches
 *    the version format, return that value.
 */
func getStableBranchName() string {
	versionFormat := regexp.MustCompile(`^(\d{1,2})\.(\d{1,2})$`)

	refName := os.Getenv("CI_COMMIT_REF_NAME")
	if match := versionFormat.FindStringSubmatch(refName); match != nil {
		return fmt.Sprintf("%s-%s-stable", match[1], match[2])
	}

	mrName := os.Getenv("CI_MERGE_REQUEST_TARGET_BRANCH_NAME")
	if match := versionFormat.FindStringSubmatch(mrName); match != nil {
		return fmt.Sprintf("%s-%s-stable", match[1], match[2])
	}

	return ""
}

/*
 * The charts versions do not follow the same GitLab major number, BUT
 * they do follow a pattern https://docs.gitlab.com/charts/installation/version_mappings/:
 *
 * 1. The minor version is the same for both
 * 2. The major version augments for both at the same time
 *
 * This means we can deduct the charts version from the GitLab version, since
 * the major charts version is always 9 versions behind its GitLab counterpart.
 */
func chartsStableBranch(gitlabStableBranch string) string {
	parts := strings.Split(gitlabStableBranch, "-")
	if len(parts) != 3 {
		return ""
	}

	major, _ := strconv.Atoi(parts[0])
	minor := parts[1]

	chartsMajor := major - 9
	return fmt.Sprintf("%d-%s-stable", chartsMajor, minor)
}

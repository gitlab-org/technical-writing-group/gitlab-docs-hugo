package tasks

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestProductCloneInfo(t *testing.T) {
	testCases := []struct {
		name            string
		productName     string
		product         Product
		envVarSuffix    string
		mergeRequestIID string
		branchName      string
		expectedBranch  string
		expectedRef     string
	}{
		{
			name:            "Hourly pipeline, or MR to gitlab-docs",
			productName:     "",
			envVarSuffix:    "",
			product:         Product{DefaultBranch: "main"},
			mergeRequestIID: "",
			branchName:      "main",
			expectedBranch:  "main",
			expectedRef:     "heads/main",
		},
		{
			name:            "Stable branch pipeline",
			productName:     "",
			envVarSuffix:    "",
			product:         Product{DefaultBranch: "main"},
			mergeRequestIID: "",
			branchName:      "16.6",
			expectedBranch:  "16.6",
			expectedRef:     "heads/16.6",
		},
		{
			name:            "Test with merge request",
			productName:     "gitlab",
			envVarSuffix:    "EE",
			product:         Product{DefaultBranch: "master"},
			mergeRequestIID: "123",
			branchName:      "feature-branch",
			expectedBranch:  "feature-branch",
			expectedRef:     "merge-requests/123/head",
		},
		{
			name:            "Test without merge request",
			productName:     "operator",
			envVarSuffix:    "OPERATOR",
			product:         Product{DefaultBranch: "main"},
			mergeRequestIID: "",
			branchName:      "",
			expectedBranch:  "main",
			expectedRef:     "heads/main",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			t.Setenv(fmt.Sprintf("MERGE_REQUEST_IID_%s", tc.envVarSuffix), tc.mergeRequestIID)
			t.Setenv(fmt.Sprintf("BRANCH_%s", tc.envVarSuffix), tc.branchName)

			actualBranch, actualRef := tc.product.CloneInfo(tc.productName)

			require.Equal(t, tc.expectedBranch, actualBranch)
			require.Equal(t, tc.expectedRef, actualRef)
		})
	}
}

func TestStableBranchFor(t *testing.T) {
	testCases := []struct {
		name           string
		productName    string
		product        Product
		envVars        map[string]string
		expectedBranch string
	}{
		{
			name:           "GitLab stable branch",
			productName:    "gitlab",
			product:        Product{DefaultBranch: "master"},
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "15.8"},
			expectedBranch: "15-8-stable-ee",
		},
		{
			name:           "Omnibus stable branch",
			productName:    "omnibus",
			product:        Product{DefaultBranch: "master"},
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "15.8"},
			expectedBranch: "15-8-stable",
		},
		{
			name:           "Runner stable branch",
			productName:    "runner",
			product:        Product{DefaultBranch: "main"},
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "15.8"},
			expectedBranch: "15-8-stable",
		},
		{
			name:           "Charts stable branch",
			productName:    "charts",
			product:        Product{DefaultBranch: "master"},
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "15.8"},
			expectedBranch: "6-8-stable",
		},
		{
			name:           "Operator default branch",
			productName:    "operator",
			product:        Product{DefaultBranch: "master"},
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "15.8"},
			expectedBranch: "master",
		},
		{
			name:           "No stable branch",
			productName:    "gitlab",
			product:        Product{DefaultBranch: "master"},
			envVars:        map[string]string{},
			expectedBranch: "master",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			for k, v := range tc.envVars {
				t.Setenv(k, v)
			}

			actualBranch := tc.product.StableBranchFor(tc.productName)

			require.Equal(t, tc.expectedBranch, actualBranch)
		})
	}
}

func TestGetStableBranchName(t *testing.T) {
	testCases := []struct {
		name           string
		envVars        map[string]string
		expectedBranch string
	}{
		{
			name:           "CI_COMMIT_REF_NAME set",
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "15.8"},
			expectedBranch: "15-8-stable",
		},
		{
			name:           "CI_MERGE_REQUEST_TARGET_BRANCH_NAME set",
			envVars:        map[string]string{"CI_MERGE_REQUEST_TARGET_BRANCH_NAME": "16.0"},
			expectedBranch: "16-0-stable",
		},
		{
			name:           "No relevant env vars set",
			envVars:        map[string]string{},
			expectedBranch: "",
		},
		{
			name:           "Invalid version format",
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "invalid"},
			expectedBranch: "",
		},
		{
			name:           "CI_COMMIT_REF_NAME takes precedence",
			envVars:        map[string]string{"CI_COMMIT_REF_NAME": "15.8", "CI_MERGE_REQUEST_TARGET_BRANCH_NAME": "16.0"},
			expectedBranch: "15-8-stable",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Clear environment before each test
			os.Clearenv()

			// Set environment variables for the test
			for k, v := range tc.envVars {
				t.Setenv(k, v)
			}

			actualBranch := getStableBranchName()

			require.Equal(t, tc.expectedBranch, actualBranch)
		})
	}
}

func TestChartsStableBranch(t *testing.T) {
	testCases := []struct {
		name               string
		gitlabStableBranch string
		expectedBranch     string
	}{
		{
			name:               "Valid GitLab stable branch",
			gitlabStableBranch: "15-8-stable",
			expectedBranch:     "6-8-stable",
		},
		{
			name:               "Another valid GitLab stable branch",
			gitlabStableBranch: "16-0-stable",
			expectedBranch:     "7-0-stable",
		},
		{
			name:               "Invalid GitLab stable branch format",
			gitlabStableBranch: "invalid-format",
			expectedBranch:     "",
		},
		{
			name:               "Empty GitLab stable branch",
			gitlabStableBranch: "",
			expectedBranch:     "",
		},
		{
			name:               "GitLab stable branch with single-digit version",
			gitlabStableBranch: "9-5-stable",
			expectedBranch:     "0-5-stable",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			actualBranch := chartsStableBranch(tc.gitlabStableBranch)

			require.Equal(t, tc.expectedBranch, actualBranch)
		})
	}
}

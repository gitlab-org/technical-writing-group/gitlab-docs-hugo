package tasks

import (
	"fmt"
	"path/filepath"

	"gopkg.in/yaml.v3"
)

func buildFeatureFlagsData(paths map[string]string) (map[string]interface{}, error) {
	// Initialize feature flags struct.
	featureFlags := map[string]interface{}{
		"products": make(map[string][]map[string]string),
	}

	products := featureFlags["products"].(map[string][]map[string]string)

	// Iterate through each feature flag path.
	for key, path := range paths {
		// Grab files in the provided path.
		files, err := filepath.Glob(path)
		if err != nil {
			return nil, fmt.Errorf("error with provided file path pattern %s: %w", path, err)
		}

		for _, featureFlagYAML := range files {
			var data map[string]string
			if err := ReadYAMLFile(featureFlagYAML, &data); err != nil {
				return nil, err
			}

			products[key] = append(products[key], data)
		}
	}

	return featureFlags, nil
}

func BuildFeatureFlags() error {
	// Create feature flag directory paths.
	featureFlagsDir := filepath.Join("..", "gitlab", "config", "feature_flags")
	featureFlagsDirAbs, err := filepath.Abs(featureFlagsDir)
	if err != nil {
		return fmt.Errorf("error getting absolute path for feature flags directory: %w", err)
	}

	featureFlagsEEDir := filepath.Join("..", "gitlab", "ee", "config", "feature_flags")
	featureFlagsEEDirAbs, err := filepath.Abs(featureFlagsEEDir)
	if err != nil {
		return fmt.Errorf("error getting absolute path for feature flags EE directory: %w", err)
	}

	// Store paths.
	paths := map[string]string{
		"GitLab Community Edition and Enterprise Edition": filepath.Join(featureFlagsDirAbs, "**", "*.yml"),
		"GitLab Enterprise Edition only":                  filepath.Join(featureFlagsEEDirAbs, "**", "*.yml"),
	}

	// Gather feature flags and save in a map.
	featureFlags, err := buildFeatureFlagsData(paths)
	if err != nil {
		return fmt.Errorf("error building feature flags data: %w", err)
	}

	// Marshal the feature flags to YAML
	yamlData, err := yaml.Marshal(featureFlags)
	if err != nil {
		return fmt.Errorf("error marshaling feature flags to YAML: %w", err)
	}

	// Create and write the file using BuildFileManager
	fileManager := NewBuildFileManager("feature_flags.yaml")
	if err := fileManager.PrepareAndWrite(yamlData); err != nil {
		return fmt.Errorf("error writing feature flags file: %w", err)
	}

	fmt.Println("Feature flags YAML file created successfully.")
	return nil
}

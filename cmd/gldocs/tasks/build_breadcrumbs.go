package tasks

import (
	"encoding/json"
	"fmt"
)

type NavItem struct {
	Title   string    `yaml:"title"`
	URL     string    `yaml:"url"`
	Submenu []NavItem `yaml:"submenu,omitempty"`
}

type Breadcrumb struct {
	Title string `json:"title"`
	Path  string `json:"path"`
}

type Page struct {
	Path        string       `json:"path"`
	Breadcrumbs []Breadcrumb `json:"breadcrumbs"`
}

func BuildBreadcrumbs() error {
	fileManager := NewBuildFileManager("breadcrumbs.json")

	// Read navigation data
	var navigation []NavItem
	if err := ReadYAMLFile("data/navigation.yaml", &navigation); err != nil {
		return fmt.Errorf("error reading navigation YAML: %w", err)
	}

	// Finds the breadcrumb trail for each page
	pages := processNavigation(navigation, []Breadcrumb{})

	// Marshal pages to JSON
	jsonData, err := json.MarshalIndent(pages, "", "  ")
	if err != nil {
		return fmt.Errorf("error marshaling JSON: %w", err)
	}

	// Write the file using the file manager
	if err := fileManager.PrepareAndWrite(jsonData); err != nil {
		return fmt.Errorf("error writing breadcrumbs file: %w", err)
	}

	fmt.Printf("breadcrumbs.json file created successfully.\n")
	return nil
}

func processNavigation(items []NavItem, parentBreadcrumbs []Breadcrumb) []Page {
	var pages []Page

	for _, item := range items {
		currentBreadcrumbs := append(parentBreadcrumbs, Breadcrumb{
			Title: item.Title,
			Path:  item.URL,
		})

		// For top-level pages (when parentBreadcrumbs is empty),
		// set the breadcrumb to the page itself
		if len(parentBreadcrumbs) == 0 {
			pages = append(pages, Page{
				Path: item.URL,
				Breadcrumbs: []Breadcrumb{{
					Title: item.Title,
					Path:  item.URL,
				}},
			})
		} else {
			pages = append(pages, Page{
				Path:        item.URL,
				Breadcrumbs: currentBreadcrumbs[:len(currentBreadcrumbs)-1],
			})
		}

		if len(item.Submenu) > 0 {
			subPages := processNavigation(item.Submenu, currentBreadcrumbs)
			pages = append(pages, subPages...)
		}
	}

	return pages
}

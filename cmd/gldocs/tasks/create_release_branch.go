package tasks

import (
	"fmt"
	"log"
	"os"
)

func CreateReleaseArchive(version string) {
	DRY_RUN := os.Getenv("DRY_RUN") == "true"

	// Abort if local branch already exists.
	if localBranchExists(version, runGitCommand) {
		log.Fatalf("ERROR: Task aborted! Local branch already exists. Run `git branch -D %s` and rerun the task.", version)
	}

	versionedDockerfile := fmt.Sprintf("%s.Dockerfile", version)
	singleDockerfile := "dockerfiles/single.Dockerfile"

	if DRY_RUN {
		log.Printf("INFO: (docs-gitlab-com): DRY RUN: Not stashing changes, checking out main branch, and pulling updates.")
		log.Printf("INFO: (docs-gitlab-com): DRY RUN: Not creating branch %s.", version)
		log.Printf("INFO: (docs-gitlab-com): DRY RUN: Not creating file %s.", versionedDockerfile)
		log.Printf("INFO: (docs-gitlab-com): DRY RUN: Not adding file %s to branch %s or committing changes.", versionedDockerfile, version)
		log.Printf("INFO: (docs-gitlab-com): DRY RUN: Not pushing branch %s.", version)
	} else {
		// Stash changes, checkout and update local main branch.
		log.Printf("INFO: (docs-gitlab-com): Stashing changes, checking out main branch, and pulling updates...")
		err := runMakeCommand("update")
		if err != nil {
			log.Fatalf("ERROR: Failed to run make command: update.")
		}

		// Create milestone branch off of main.
		log.Printf("INFO: (docs-gitlab-com): Creating branch %s...", version)
		_, err = runGitCommand("checkout", "-b", version)
		if err != nil {
			log.Fatalf("ERROR: Failed to create branch %s.", version)
		}

		// Creates versioned Dockerfile if it does not exist/user overwrites existing.
		log.Printf("INFO: (docs-gitlab-com): Creating versioned Dockerfile.")
		createDockerfile(versionedDockerfile, singleDockerfile)

		// Pushing up local changes to version branch.
		log.Printf("INFO: (docs-gitlab-com): Adding file %s, committing changes, and pushing to branch %s...", versionedDockerfile, version)
		pushChangesToRemote([]string{fmt.Sprintf("%s.Dockerfile", version)}, fmt.Sprintf("Release cut %s", version), version, map[string]string{})

		log.Printf("Stable branch created successfully.")
	}
}

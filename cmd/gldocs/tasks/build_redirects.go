package tasks

import (
	"fmt"
	"strings"
)

// Redirect represents a single redirect entry in the YAML file.
type Redirect struct {
	From       string `yaml:"from"`
	To         string `yaml:"to"`
	RemoveDate string `yaml:"remove_date,omitempty"`
}

// RedirectsFile represents the structure of the redirects YAML file.
type RedirectsFile struct {
	Redirects []Redirect `yaml:"redirects"`
}

// BuildRedirects generates the GitLab Pages _redirects file from the YAML source.
func BuildRedirects() error {
	bfm := &BuildFileManager{
		OutputPath: "static/_redirects",
		DataDir:    "static",
	}

	// Read and parse the YAML file
	var redirectsFile RedirectsFile
	if err := ReadYAMLFile("data/redirects.yaml", &redirectsFile); err != nil {
		return err
	}

	// Generate redirects content
	var redirectsContent strings.Builder
	for _, redirect := range redirectsFile.Redirects {
		redirectsContent.WriteString(fmt.Sprintf("%s %s 301\n", redirect.From, redirect.To))
	}

	// Prepare directory, cleanup existing file, and write new content
	if err := bfm.PrepareAndWrite([]byte(redirectsContent.String())); err != nil {
		return err
	}

	fmt.Println("_redirects file created successfully.")
	return nil
}

package tasks

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"
)

func TestProcessNavigation(t *testing.T) {
	testCases := []struct {
		name           string
		input          string
		expectedOutput []Page
	}{
		{
			name: "Simple navigation",
			input: `
- title: Lorem Ipsum
  url: 'dolor/sit/'
  submenu:
    - title: Amet Consectetur
      url: 'dolor/sit/amet/'
    - title: Adipiscing Elit
      url: 'dolor/sit/elit/'
`,
			expectedOutput: []Page{
				{
					Path: "dolor/sit/",
					Breadcrumbs: []Breadcrumb{
						{Title: "Lorem Ipsum", Path: "dolor/sit/"},
					},
				},
				{
					Path: "dolor/sit/amet/",
					Breadcrumbs: []Breadcrumb{
						{Title: "Lorem Ipsum", Path: "dolor/sit/"},
					},
				},
				{
					Path: "dolor/sit/elit/",
					Breadcrumbs: []Breadcrumb{
						{Title: "Lorem Ipsum", Path: "dolor/sit/"},
					},
				},
			},
		},
		{
			name: "Nested navigation",
			input: `
- title: Sed Do Eiusmod
  url: 'tempor/incididunt/'
  submenu:
    - title: Ut Labore
      url: 'tempor/incididunt/labore/'
    - title: Et Dolore
      url: 'tempor/incididunt/dolore/'
      submenu:
        - title: Magna Aliqua
          url: 'tempor/incididunt/dolore/magna/'
`,
			expectedOutput: []Page{
				{
					Path: "tempor/incididunt/",
					Breadcrumbs: []Breadcrumb{
						{Title: "Sed Do Eiusmod", Path: "tempor/incididunt/"},
					},
				},
				{
					Path: "tempor/incididunt/labore/",
					Breadcrumbs: []Breadcrumb{
						{Title: "Sed Do Eiusmod", Path: "tempor/incididunt/"},
					},
				},
				{
					Path: "tempor/incididunt/dolore/",
					Breadcrumbs: []Breadcrumb{
						{Title: "Sed Do Eiusmod", Path: "tempor/incididunt/"},
					},
				},
				{
					Path: "tempor/incididunt/dolore/magna/",
					Breadcrumbs: []Breadcrumb{
						{Title: "Sed Do Eiusmod", Path: "tempor/incididunt/"},
						{Title: "Et Dolore", Path: "tempor/incididunt/dolore/"},
					},
				},
			},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var navigation []NavItem
			err := yaml.Unmarshal([]byte(tc.input), &navigation)
			require.NoError(t, err, "Error parsing YAML")

			result := processNavigation(navigation, []Breadcrumb{})
			require.Equal(t, tc.expectedOutput, result, "Unexpected result from processNavigation")
		})
	}
}

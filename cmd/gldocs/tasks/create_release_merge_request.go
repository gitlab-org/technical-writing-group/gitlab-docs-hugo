package tasks

import (
	"fmt"
	"log"
	"strings"
)

type Version struct {
	Next      string   `json:"next"`
	Current   string   `json:"current"`
	LastMinor []string `json:"last_minor"`
	LastMajor []string `json:"last_major"`
}

func CreateMergeRequest(version string) {
	// Stash local changes, checkout main branch, pull any new changes.
	log.Printf("INFO: (docs-gitlab-com): Stash and update main branch.")
	err := runMakeCommand("update")
	if err != nil {
		log.Fatalf("ERROR: Failed to run make command.")
	}

	// Create new version branch.
	log.Printf("INFO: (docs-gitlab-com): Create release branch.")
	branch := "release-" + strings.ReplaceAll(version, ".", "-")
	_, err = runGitCommand("checkout", "-b", branch)
	if err != nil {
		log.Fatalf("ERROR: Failed to pull main branch.")
	}

	// Update version.json with new versions.
	log.Printf("INFO: (docs-gitlab-com): Update versions.json file.")
	updaterVersionsFile(version)

	// Update docker-images.gitlab-ci.yml.
	log.Printf("INFO: (docs-gitlab-com): Update docker-images.gitlab-ci.yml file.")
	updateDockerImagesYML(version)

	// Commit and push the changes.
	log.Printf("INFO: (docs-gitlab-com): Stage and push changes to branch.")
	// Commit message.
	commitMsg := fmt.Sprintf("Docs release %s", version)
	// Environment variables map.
	envVars := map[string]string{"LEFTHOOK": "0"}
	pushChangesToRemote([]string{}, commitMsg, branch, envVars)

	// Login and create merge request for these changes.
	log.Printf("INFO: (docs-gitlab-com): Create merge request.")
	glabAuthAndMRCreation(branch, commitMsg)
}

package tasks

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"github.com/hashicorp/go-version"
)

// Checks if current milestone branch already exists locally.
func localBranchExists(branch string, runGitCmd func(...string) (string, error)) bool {
	output, err := runGitCmd("branch", "--list", branch)
	if err != nil {
		return false
	}
	return strings.TrimSpace(output) != ""
}

// Checks if file exists.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// Checks if user wants to overwrite a file.
func askToOverwrite(filename string) bool {
	log.Printf("%s already exists. Do you want to overwrite? (y/n): ", filename)
	reader := bufio.NewReader(os.Stdin)
	response, _ := reader.ReadString('\n')
	// Trim any extra space and cast response to lowercase.
	response = strings.TrimSpace(strings.ToLower(response))
	return response == "y"
}

// Run a "make" command.
func runMakeCommand(target string) error {
	cmd := exec.Command("make", target)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// Pushes local changes to remote.
func pushChangesToRemote(files []string, commitMsg string, branch string, envVars map[string]string) {
	// Stage files.
	args := append([]string{"add"}, files...)
	if len(files) == 0 {
		args = []string{"add", "."}
	}
	if _, err := runGitCommand(args...); err != nil {
		log.Fatalf("ERROR: Failed to stage files. Error: %v.", err)
	}

	// Commit changes.
	_, err := runGitCommand("commit", "-m", commitMsg)
	if err != nil {
		log.Fatalf("ERROR: Failed to commit files: %+v.", files)
	}

	// Push changes.
	pushChangesCmd := exec.Command("git", "push", "-u", "origin", branch)

	// Set environment variables if any.
	env := os.Environ()
	for key, value := range envVars {
		env = append(env, fmt.Sprintf("%s=%s", key, value))
	}
	pushChangesCmd.Env = env

	var stdout, stderr bytes.Buffer
	pushChangesCmd.Stdout = &stdout
	pushChangesCmd.Stderr = &stderr

	err = pushChangesCmd.Run()
	if err != nil {
		errMsg := stderr.String()
		if errMsg == "" {
			errMsg = stdout.String() // To catch errors that are outputted to stdout.
		}
		log.Fatalf("ERROR: Failed to push changes to branch: %s.\nGit error output: %s.", branch, errMsg)
	}
}

// Returns next version.
func nextVersion(currentVersion string) (string, error) {
	// Parse currentVersion string.
	ver, err := version.NewVersion(currentVersion)
	if err != nil {
		return "", fmt.Errorf("Failed to parse current version.")
	}

	// Get the major/minor segments of the currentVersion.
	segments := ver.Segments()

	// Assing major and minor segments.
	majorVer := segments[0]
	minorVer := segments[1]

	// Increment the version.
	if minorVer == 11 {
		majorVer++
		minorVer = 0
	} else {
		minorVer++
	}

	// Construct the new "next version".
	nextVersion := fmt.Sprintf("%d.%d", majorVer, minorVer)

	return nextVersion, nil
}

// Returns an array of last major version and second last major version calculated from latest version.
func getLastMajorVersions(currentVersion string, lastMajorVersion string, secondLastMajorVersion string) []string {
	currentMajorVersion, _ := strconv.Atoi(strings.Split(currentVersion, ".")[0])
	lastMajorVersionInt, _ := strconv.Atoi(strings.Split(lastMajorVersion, ".")[0])

	// If current major version is more than 1 greater than the last major version, increase both last major entries by one.
	if currentMajorVersion > lastMajorVersionInt+1 {
		newLastMajor := fmt.Sprintf("%d.11", lastMajorVersionInt+1)
		newSecondLastMajor := fmt.Sprintf("%d.11", lastMajorVersionInt)
		return []string{newLastMajor, newSecondLastMajor}
	}

	// If current major version is not greater than the last major version, keep the same last major array.
	return []string{lastMajorVersion, secondLastMajorVersion}
}

// Creates a versioned Dockerfile off of single.Dockerfile
func createDockerfile(versionedDockerfile string, singleDockerfile string) {
	if fileExists(versionedDockerfile) && !askToOverwrite(versionedDockerfile) {
		// If dockerfile already exists, and if user does not want to overwrite the file, abort.
		log.Fatalf("ERROR: Aborting %s overwrite.", versionedDockerfile)
	} else {
		log.Printf("INFO: (docs-gitlab-com): Creating file %s.", versionedDockerfile)
		// Copy single.Dockerfile to a version specific dockerfile.
		cpFileCmd := exec.Command("cp", singleDockerfile, versionedDockerfile)
		err := cpFileCmd.Run()
		if err != nil {
			log.Fatalf("ERROR: Failed to create version Dockerfile: %s.", versionedDockerfile)
		}
	}
}

// Authorize user through GitLab CLI and create merge request.
func glabAuthAndMRCreation(branch string, titleDesc string) {
	// Check current auth status.
	authStatusCmd := exec.Command("glab", "auth", "status")
	var stderr bytes.Buffer

	// The output goes to error.
	authStatusCmd.Stderr = &stderr
	err := authStatusCmd.Run()
	if err != nil {
		log.Fatalf("ERROR: Failed running glab auth status command. Error: %v.", err)
	}

	// Check if we need to authenticate user.
	if strings.Contains(stderr.String(), "Logged in to") {
		log.Println("Already authenticated to GitLab CLI.")
	} else {
		// If not authenticated, perform login.
		authCmd := exec.Command("glab", "auth", "login", "-h", "gitlab.com")
		err := authCmd.Run()
		if err != nil {
			log.Fatalf("ERROR: Failed to login to GitLab CLI.")
		}
		log.Println("Successfully authenticated to GitLab CLI.")
	}

	mrCmd := exec.Command("glab", "mr", "create", "-t", titleDesc, "-d", titleDesc, "--label", "release")
	output, err := mrCmd.CombinedOutput()
	if err != nil {
		log.Fatalf("ERROR: Failed to create merge request for branch %s. Error: %v.", branch, err)
	}

	// Convert output to string.
	outputStr := string(output)

	// Use a regular expression to find the merge request URL.
	re := regexp.MustCompile(`https://[^\s]+`)
	mrURL := re.FindString(outputStr)

	if mrURL == "" {
		log.Println("WARNING: Docs release merge request created successfully but could not find merge request URL in the output.")
	} else {
		log.Printf("Docs release merge request created successfully. Link: %s.\n", mrURL)
	}

}

// Update versions.json file.
func updaterVersionsFile(version string) {
	// Update versions.json file.
	versionsFile := "content/versions.json"
	// Open versions file.
	jsonFile, err := os.Open(versionsFile)
	if err != nil {
		log.Fatalf("ERROR: Failed to open versions.json.")
	}
	defer jsonFile.Close()

	byteValue, _ := io.ReadAll(jsonFile)

	// Unmarshal JSON data into a slice of Version structs.
	var versions []Version
	err = json.Unmarshal(byteValue, &versions)
	if err != nil {
		log.Fatalf("ERROR: Failed to unmarshal version.json data.")
	}

	// Calculated next version.
	nextVersion, err := nextVersion(version)
	if err != nil {
		log.Fatalf("ERROR: %s.", err)
	}

	// Modify versions.json data.
	for i := range versions {
		// Previous current.
		prevCurrentVersion := versions[i].Current
		// Set last minor.
		recentMinorRelease := versions[i].LastMinor[0]
		versions[i].LastMinor = []string{prevCurrentVersion, recentMinorRelease}
		// Update next release.
		versions[i].Next = nextVersion
		// Update version being released to current.
		versions[i].Current = version
		// Update last major versions.
		versions[i].LastMajor = getLastMajorVersions(version, versions[i].LastMajor[0], versions[i].LastMajor[1])
	}
	// Marshal the modified data back to JSON.
	modifiedJSON, err := json.MarshalIndent(versions, "", "  ")
	if err != nil {
		log.Fatalf("ERROR: Failed marshalling JSON.")
	}

	// Write the modified JSON back to the file.
	err = os.WriteFile(versionsFile, modifiedJSON, 0600)
	if err != nil {
		log.Fatalf("ERROR: Failed to update versions.json.")
	}
}

// Updates docker-images.gitlab-ci.yml.
func updateDockerImagesYML(version string) {
	dockerImgYML := ".gitlab/ci/docker-images.gitlab-ci.yml"
	content, err := os.ReadFile(dockerImgYML)
	if err != nil {
		log.Fatalf("ERROR: Failed to read %s.", dockerImgYML)
	}
	gitlabVer := regexp.MustCompile(`(?m)^(\s*)GITLAB_VERSION:.*$`)
	newContent := gitlabVer.ReplaceAllString(string(content), "${1}GITLAB_VERSION: '"+version+"'")
	err = os.WriteFile(dockerImgYML, []byte(newContent), 0644)
	if err != nil {
		log.Fatalf("ERROR: Failed to write %s.", dockerImgYML)
	}
}

package main

import "gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/cmd"

func main() {
	cmd.Execute()
}

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/cmd/gldocs/tasks"
)

var buildCmd = &cobra.Command{
	Use:   "build",
	Short: "Build dynamic content for the Docs site.",
	Long:  `Build dynamic content for the Docs site. See https://docs.gitlab.com/development/documentation/site_architecture/automation/ for more information.`,
	RunE:  buildRun,
}

func init() {
	rootCmd.AddCommand(buildCmd)
}

func buildRun(cmd *cobra.Command, args []string) error {
	if err := tasks.BuildBreadcrumbs(); err != nil {
		return fmt.Errorf("failed to build breadcrumbs: %w", err)
	}

	if err := tasks.BuildFeatureFlags(); err != nil {
		return fmt.Errorf("failed to build feature flags: %w", err)
	}

	if err := tasks.BuildRedirects(); err != nil {
		return fmt.Errorf("failed to build redirects: %w", err)
	}

	return nil
}

# GitLab Docs build and linting Docker image
#
# Value for HUGO_VERSION is defined in .gitlab-ci.yml
ARG HUGO_VERSION=latest

FROM hugomods/hugo:node-lts-${HUGO_VERSION}

# Install dependencies
RUN printf "\n\e[32mINFO: Installing dependencies..\e[39m\n" \
  && apk update && apk upgrade --no-cache && apk add --no-cache bash git go golangci-lint make shellcheck yq \
  && printf "\n\e[32mINFO: Dependency versions:\e[39m\n" \
  && echo "Git: $(git --version)" \
  && echo "Go: $(go version)" \
  && echo "golangci-lint: $(golangci-lint version)" \
  && echo "Node.js: $(node --version)" \
  && echo "Yarn: $(yarn --version)" \
  && echo "yq: $(yq --version)" \
  && printf "\n"

# Enable corepack
RUN corepack enable

# Copy this Dockerfile to the root of each branch you want to create an archive
# and rename it to X.Y.Dockerfile, where X.Y the major.minor GitLab version.
#

# Global variables to be used with FROM
ARG HUGO_VERSION

FROM hugomods/hugo:exts-${HUGO_VERSION} AS builder

ARG CI_COMMIT_REF_NAME
ENV CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}

ARG COREPACK_VERSION

ENV HUGO_BASEURL=https://docs.gitlab.com/${CI_COMMIT_REF_NAME}
ENV HUGO_PUBLISHDIR=public/${CI_COMMIT_REF_NAME}

# Install dependencies and tools that
# are needed to build the docs site and run the tests.
RUN apk add --no-cache \
    curl        \
    coreutils   \
    make        \
    bash        \
    gcc         \
    libc-dev    \
    yq-go       \
    nodejs

COPY . /source/
WORKDIR /source

RUN node --version \
    # https://github.com/nodejs/corepack?tab=readme-ov-file#manual-installs
    && npm uninstall -g yarn pnpm                   \
    && npm install -g corepack@${COREPACK_VERSION}  \
    && corepack enable                              \
    && yarn --version                               \
    && yarn install --immutable                     \
    && make clone-docs-projects                     \
    && make build-archive                           \
    && find public -type f -regex '.*\.\(css\|html\|js\|txt\|xml\)$' -exec gzip -f -k {} \;

#- End of builder build stage -#

#- Start of NGINX stage -#
#
# Copy the ending HTML files from the previous 'builder' stage and copy them
# to an NGINX Docker image.
FROM nginx:stable-alpine

ARG CI_COMMIT_REF_NAME
ENV CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}

# Clean out any existing HTML files
RUN rm -rf /usr/share/nginx/html/*

# Copy the HTML from the builder stage to the default location for NGINX.
# The trailing slashes of the source and destination directories matter.
# https://docs.docker.com/engine/reference/builder/#copy
COPY --from=builder /source/public/ /usr/share/nginx/html/

# Make an index.html and 404.html which will redirect / to /${CI_COMMIT_REF_NAME}/
RUN echo "<html><head><title>Redirect for ${CI_COMMIT_REF_NAME}</title><meta http-equiv=\"refresh\" content=\"0;url='/${CI_COMMIT_REF_NAME}/'\" /></head><body><p>If you are not redirected automatically, click <a href=\"/${CI_COMMIT_REF_NAME}/\">here</a>.</p></body></html>" > /usr/share/nginx/html/index.html \
    && echo "<html><head><title>Redirect for ${CI_COMMIT_REF_NAME}</title><meta http-equiv=\"refresh\" content=\"0;url='/${CI_COMMIT_REF_NAME}/'\" /></head><body><p>If you are not redirected automatically, click <a href=\"/${CI_COMMIT_REF_NAME}/\">here</a>.</p></body></html>" > /usr/share/nginx/html/404.html

# Copy the NGINX config
COPY dockerfiles/nginx-overrides.conf /etc/nginx/conf.d/default.conf

# Start NGINX to serve the archive at / (which will redirect to the version-specific dir)
CMD ["sh", "-c", "echo 'GitLab Docs are viewable at: http://0.0.0.0:4000/${CI_COMMIT_REF_NAME}'; exec nginx -g 'daemon off;'"]

#- End of NGINX stage -#

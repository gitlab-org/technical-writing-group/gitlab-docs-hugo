INFO = \033[32m
WARN = \033[31m
END = \033[0m

include makefiles/*.mk

.PHONY: update
update:
	@printf "\n$(INFO)INFO: Stashing any changes, switching to main branch, and pulling updates to GitLab Docs Hugo project...$(END)\n"
	@git stash && git checkout main && git pull --ff-only

.PHONY: install-dependencies
install-dependencies:
	@printf "\n$(INFO)INFO: Installing and updating dependencies...$(END)\n"
	@scripts/install-dependencies.sh

.PHONY: install-nodejs-dependencies
install-nodejs-dependencies:
	@printf "\n$(INFO)INFO: Installing Node.js packages...$(END)\n"
	@yarn install

.PHONY: clone-docs-projects
clone-docs-projects:
	@printf "\n$(INFO)INFO: Fetching docs content sources...$(END)\n"
	@go run cmd/gldocs/main.go clone
	@printf "\n$(INFO)INFO: Building dynamic content...$(END)\n"
	@go run cmd/gldocs/main.go build
	@printf "\n$(INFO)INFO: Adding latest GitLab SVGs...$(END)\n"
	@make add-latest-icons

.PHONY: add-latest-icons
add-latest-icons:
	@scripts/add-latest-icons.sh
	@if [[ ! -s "themes/gitlab-docs/static/gitlab_ui/svgs-latest/icons.svg" ]] || \
		[[ ! -s "data/icons.json" ]]; then \
		echo "Error: SVG update failed - required files are missing or empty" >&2; \
		exit 1; \
	fi

.PHONY: check-redirect-threshold
check-redirect-threshold:
	@printf "\n$(INFO)INFO: Checking redirect threshold...$(END)\n"
	@scripts/redirect-threshold-check.sh

.PHONY: all
all: clean setup test

.PHONY: clean
clean:
	@printf "\n$(INFO)INFO: Removing ephemeral directories...$(END)\n"
	@rm -rfv public resources data/icons.json data/breadcrumbs.json data/feature_flags.yaml themes/gitlab-docs/static/icons.svg themes/gitlab-docs/static/vite themes/gitlab-docs/static/gitlab_ui static/_redirects

.PHONY: setup
setup: install-dependencies install-nodejs-dependencies add-latest-icons

.PHONY: test
test: lint-markdown lint-shell-scripts lint-go lint-frontend lint-yaml go-tests jest-tests markdown-link-tests

.PHONY: create-stable-branch
create-stable-branch:
	@if [ -z "$(DRY_RUN)" ] || [ "$(DRY_RUN)" = "false" ]; then \
		printf "\n$(INFO)INFO: Creating stable branch without DRY_RUN environment variable...$(END)\n"; \
		go run cmd/gldocs/main.go release $(VERSION); \
	else \
		printf "\n$(INFO)INFO: Creating stable branch with DRY_RUN environment variable...$(END)\n"; \
		DRY_RUN=${DRY_RUN} go run cmd/gldocs/main.go release $(VERSION); \
	fi

.PHONY: create-release-merge-request
create-release-merge-request:
	@printf "\n$(INFO)INFO: Creating merge request with updated versions.json...$(END)\n"; \
	go run cmd/gldocs/main.go releaseMR $(VERSION)

.PHONY: build-frontend
build-frontend:
	@printf "\n$(INFO)INFO: Building documentation site frontend...$(END)\n"; \
	yarn build

.PHONY: build
build: add-latest-icons build-frontend
	@printf "\n$(INFO)INFO: Building documentation site...$(END)\n"; \
	hugo --gc --minify  --printPathWarnings --panicOnWarning

.PHONY: view
view: add-latest-icons build-frontend
	@printf "\n$(INFO)INFO: Building documentation site with preview...$(END)\n"; \
	hugo serve -O

.PHONY: view-archive

BASEURL := https://docs.gitlab.com/${CI_COMMIT_REF_NAME}
PUBLISHDIR := public/${CI_COMMIT_REF_NAME}

view-archive:
	@if [ -z "$(CI_COMMIT_REF_NAME)" ]; then \
		echo "$(WARN)ERROR: CI_COMMIT_REF_NAME variable not set. Set this to the expected version number.$(END)\n\nExample:\nCI_COMMIT_REF_NAME=14.10 make view-archive\n"; \
		exit 1; \
	fi
	@echo "Using CI_COMMIT_REF_NAME: ${CI_COMMIT_REF_NAME}"

	yarn build-archive

	@export HUGO_BASEURL="${BASEURL}" HUGO_PUBLISHDIR="${PUBLISHDIR}" && \
	hugo && \
	yarn pagefind --site "${PUBLISHDIR}" && \
	SEARCH_BACKEND="pagefind" hugo serve -O

.PHONY: build-archive

BASEURL := https://docs.gitlab.com/${CI_COMMIT_REF_NAME}
PUBLISHDIR := public/${CI_COMMIT_REF_NAME}

build-archive:
	@if [ -z "$(CI_COMMIT_REF_NAME)" ]; then \
		echo "$(WARN)ERROR: CI_COMMIT_REF_NAME variable not set. Set this to the expected version number.$(END)\n\nExample:\nCI_COMMIT_REF_NAME=14.10 make view-archive\n"; \
		exit 1; \
	fi
	@echo "Using CI_COMMIT_REF_NAME: ${CI_COMMIT_REF_NAME}"

	yarn build-archive

	@export HUGO_BASEURL="${BASEURL}" HUGO_PUBLISHDIR="${PUBLISHDIR}" SEARCH_BACKEND="pagefind" && \
	hugo && \
	yarn pagefind --site "${PUBLISHDIR}"

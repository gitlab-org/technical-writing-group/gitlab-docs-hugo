.PHONY: go-tests
go-tests:
	@printf "\n$(INFO)INFO: Running Go tests...$(END)\n"
	@go test ./cmd/gldocs/tasks/...

.PHONY: jest-tests
jest-tests: install-nodejs-dependencies
	@printf "\n$(INFO)INFO: Running JavaScript tests...$(END)\n"
	@yarn test

# See also test:markdown-links job in .gitlab/ci/test.gitlab-ci.yml
.PHONY: markdown-link-tests
markdown-link-tests:
	@printf "\n$(INFO)INFO: Running Markdown link tests...$(END)\n"
	@lychee --version
	@lychee --offline --include-fragments README.md doc/*.md

.PHONY: check-global-navigation
check-global-navigation:
	@printf "${INFO}Checking global navigation...${END}\n"
	@scripts/check-navigation.sh

.PHONY: check-pages-not-in-nav
check-pages-not-in-nav:
	@printf "\n$(INFO)INFO: Checking for pages missing from global navigation...$(END)\n"
	@scripts/pages_not_in_nav.cjs

.PHONY: check-codeblocks-language-support
check-codeblocks-language-support:
	@printf "\n$(INFO)INFO: Checking for codeblocks with unsupported languages...$(END)\n"
	@scripts/check_codeblocks.cjs

.PHONY: check-elastic-key-permissions
check-elastic-key-permissions:
	@printf "\n$(INFO)INFO: Checking Elastic API key permissions...$(END)\n\n"
	@scripts/check_elastic_key_permissions.cjs

.PHONY: check-analytics
check-analytics:
	@printf "\n$(INFO)INFO: Checking for analytics scripts in index.html...$(END)\n"
	@scripts/check-analytics.sh

SEARCH_DIR?=
.PHONY: check-index-pages
check-index-pages:
	@printf "\n$(INFO)INFO: Checking for invalid index filenames...$(END)\n"
	@scripts/check-index-filenames.sh $(SEARCH_DIR)

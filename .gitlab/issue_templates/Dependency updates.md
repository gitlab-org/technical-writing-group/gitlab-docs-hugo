## Dependency updates

Dependencies can be updated in a single MR, or if anything seems risky or requires
more changes to our custom code, you can create separate MRs for individual packages,
or to split up frontend/backend updates.

Take the time to review release notes for any major releases.
There may be new features of a package we might want to use,
bugfixes that allow us to remove workarounds we'd introduced previously,
or opportunities to drop some dependencies altogether.

### Node modules

- [ ] Run `npx npm-check-updates -u -x '*vue*, vite'` to bump versions for everything except Vue and Vite.
- [ ] Run `yarn` to update `yarn.lock`.
- [ ] Run `make lint-frontend` and `make jest-tests` and verify frontend tests still pass.
- [ ] Run `make view` and browse around the site a bit. Check for new console errors or other problems.

Be sure to review [`@gitlab/ui`](https://gitlab.com/gitlab-org/gitlab-ui/-/releases)
and [`mermaid`](https://github.com/mermaid-js/mermaid/releases) release notes for impactful changes.
Breaking changes are only included in major releases, but for these packages,
changes in minor versions also have the potential to make visual changes to the Docs site.

### Yarn

[Release History](https://github.com/yarnpkg/berry/releases)

- [ ] Run `yarn set version stable` to update the `packageManager` field in `package.json`.

### Go version

[Release History](https://go.dev/doc/devel/release)

- [ ] Update `.tool-versions`
- [ ] If this is a major version, update `go.mod`
- [ ] If this is a major version, update `GO_VERSION_PREVIOUS` and `GO_VERSION` in `.gitlab-ci.yml`
- [ ] If this is a major version, update the versions in `test:go > parallel > matrix > GO_VERSION` in `test.gitlab-ci.yml`
- [ ] Check that linting and tests still pass: `make lint-go && make go-tests`

### Go modules and linters

- [ ] Run `go get -t -u ./...` to update all dependencies, including test dependencies
- [ ] Run `go mod tidy` to remove now unneeded dependencies from `go.sum`
- [ ] Update `.tool-versions` and `.gitlab-ci.yml` to bump the version for `golangci-lint` ([Release History](https://github.com/golangci/golangci-lint/releases))
- [ ] Check that linting and tests still pass: `make lint-go && make go-tests`

### Linux distros

Update the relevant variable in `.gitlab-ci.yml` (e.g, `ALPINE_VERSION`). Verify pipelines all still pass.

- [ ] Alpine ([releases](https://alpinelinux.org/releases/))
  - [ ] yq is packaged with Alpine as `yq-go`. Set the yq version in `.tool-versions` with the
  appropriate version number for the Alpine version we use.
  See [alpinelinux.org](https://pkgs.alpinelinux.org/packages?name=yq-go&branch=v3.20&repo=&arch=x86_64&origin=&flagged=&maintainer=)
  for version info.
- [ ] Debian ([releases](https://www.debian.org/releases/stable/))

### Other tools

Update `.tool-versions` and the relevant variable in `.gitlab-ci.yml` (e.g, `HUGO_VERSION`).

- [ ] Hugo ([releases](https://github.com/gohugoio/hugo/releases))
- [ ] node.js ([releases](https://nodejs.org/en/download/package-manager)). We want to use the LTS version of node.
- [ ] Shellcheck ([releases](https://github.com/koalaman/shellcheck/releases))
- [ ] hadolint ([releases](https://github.com/hadolint/hadolint/releases))

Update `.gitlab-ci.yml` variables only:

- [ ] Lychee ([releases](https://github.com/lycheeverse/lychee/releases))
- [ ] asdf ([releases](https://github.com/asdf-vm/asdf/releases))
- [ ] corepack ([releases](https://www.npmjs.com/package/corepack))

### Docker images

#### `lint-markdown`

The [`lint-markdown` Docker image](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/container_registry/8246092)
contains version-controlled installs of markdownlint, Vale, and Lychee.

When we update these, we need to update the container image, and then update
references to it, which are in each of the docs content projects.

#### Hugo version

The Hugo version is referenced in CI jobs that run in all projects
that publish to `docs.gitlab.com`. When bumping the Hugo version, you
will also need to update references to it in these projects.

See [`docs hugo_build`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/docs.gitlab-ci.yml) job in `gitlab`
as an example.

Updates for the `lint-markdown` and Hugo image can be handled in a
single MR in each project.

/label ~type::maintenance ~maintenance::dependencies ~"Category:Docs Site" ~"Technical Writing"

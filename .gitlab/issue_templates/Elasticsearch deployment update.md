## Elasticsearch deployment upgrade

Before updating Elasticsearch cloud deployment, make sure to review [release notes](https://www.elastic.co/guide/en/elasticsearch/reference/current/es-release-notes.html).

Breaking changes are only introduced in major versions.
Check [migrations](https://www.elastic.co/guide/en/elasticsearch/reference/current/breaking-changes.html)
for notes on breaking changes.

Take the time to review release notes for any major releases.
There may be new features we might want to use on the cloud deployment dashboard.

### First time only

#### Set up test deployment

To test upgrades, we need to setup a test deployment called `gitlab-docs-website-test`.
We need to [request](https://gitlab.com/gitlab-com/gl-infra/observability/team/-/issues/4080) the Infrastructure team to
create one for us.

#### Set up web crawler in test deployment

1. Add web crawler and index site content.
    - Visit [Elastic cloud home page](https://cloud.elastic.co/home), look for deployment called `gitlab-docs-website-test`
    in `Hosted deployments` and click `Open` to open up Elasticsearch cloud instance.
    - Using the menu, go to Elasticsearch option and click on `Web crawlers`.
    - Create new web crawler: `search-gitlab-docs-test`.
    - Add domain: `https://docs.gitlab.com`.
    - Set up crawler scheduling for `hourly` (they run sequentially, make sure to match the order in production crawl rules).
    - Run a crawl and make sure it completes without errors and documents are indexed.

#### Set up behavioral analytics

1. [Create collection](https://gitlab-docs-website-nonprod.kb.us-central1.gcp.cloud.es.io/app/enterprise_search/analytics)
with name `search-gitlab-docs-test-collection`.
    - This will create the endpoint to connect to the analytics dashboard (i.e. `ELASTIC_ANALYTICS_ENDPOINT` in constants.js).

### Upgrading and local testing test deployment

#### Test: Upgrade

1. [ ] Go to Elasticsearch [deployments](https://cloud.elastic.co/deployments).
1. [ ] Click the deployment with name `gitlab-docs-website-test`.
1. [ ] Take a snapshot before upgrade:
    - Under `Elasticsearch` > `Snapshots`, click on `Take snapshot now` option.
    - Check [list of snapshots](https://gitlab-docs-website-nonprod.kb.us-central1.gcp.cloud.es.io/app/management/data/snapshot_restore/snapshots)
    to confirm your latest snapshot.
1. [ ] In the `Deployment version` section, click `Upgrade`.
1. [ ] Select latest version.
1. [ ] Click `Upgrade` and then `Confirm upgrade`. The deployment should take only a few minutes to create.
1. [ ] [Start a web crawl](https://gitlab-docs-website-nonprod.kb.us-central1.gcp.cloud.es.io/app/enterprise_search/content/search_indices/search-gitlab-docs-test)
 after upgrade.
1. [ ] Go to [local testing section](#test-local-testing) to test elastic cloud upgrade in test cluster.

#### Test: Local testing

1. First time only steps:
    - Create a [read-only API key](https://www.elastic.co/guide/en/search-ui/current/tutorials-elasticsearch.html#tutorials-elasticsearch-setting-up-a-read-only-api-key).
    - [Enable CORS](https://www.elastic.co/guide/en/search-ui/current/tutorials-elasticsearch.html#tutorials-elasticsearch-enabling-cors).
1. Connect to test cluster through connector:
    - Copy `CLOUD_ID` from test deployment's [Search homepage](https://gitlab-docs-website-nonprod.kb.us-central1.gcp.cloud.es.io/app/enterprise_search/overview).
    - In the project, change `ELASTIC_CLOUD_ID` in `constants.js` file to test instance's `CLOUD_ID` copied from previous
    step.
    - Other environment variables must be stored in your local environment through your desired shell configuration file
    (i.e. ~/.zshrc):
      - `ELASTIC_KEY`: the API key created in step 1.
      - `ELASTIC_INDEX`: the index that was created during the [web crawler setup](#set-up-web-crawler-in-test-deployment).
1. Connect to behavioral analytics dashboard:
    - Update constants.js file with the following:
      - `ELASTIC_ANALYTICS_ENDPOINT`: Endpoint value from [`Initialize the client`](https://gitlab-docs-website-nonprod.kb.us-central1.gcp.cloud.es.io/app/enterprise_search/analytics/collections/search-gitlab-docs-test-collection/integrate).
      - `ELASTIC_ANALYTICS_COLLECTION_NAME`: Collection name from [`Initialize the client`](https://gitlab-docs-website-nonprod.kb.us-central1.gcp.cloud.es.io/app/enterprise_search/analytics/collections/search-gitlab-docs-test-collection/integrate).

1. Start up local environment and make sure search is still working as expected and analytics dashboard is updating
metrics data (i.e. search queries and link clicks).

#### Test: Rollback version

Elasticsearch does not support version downgrades. Do the following to manually downgrade using new deployment.

1. Open up an [issue](https://gitlab.com/gitlab-com/gl-infra/observability/team/) to shutdown `gitlab-docs-website-test`
and create a new deployment with same name.
1. Go to the new cloud deployment for `gitlab-docs-website-test` > Elasticsearch > Snapshots.
1. Select `Restore from another deployment` option.
    - Restore from: `gitlab-docs-website-test`.
    - Snapshot ID of the last successful snapshot with older deployment version: Snapshot name.
1. Make sure search is working again [locally](#test-local-testing).
1. If local is now broken, refer to [release notes](https://www.elastic.co/guide/en/elasticsearch/reference/current/es-release-notes.html)
and check [Elasticsearch forum](https://discuss.elastic.co/) for additional guidance.

### Upgrading and local testing production deployment

#### Production: Upgrade

1. [ ] [Local testing](#test-local-testing) in test deployment passed with upgrade.
1. [ ] Select `gitlab-docs-website` deployment in [deployments](https://cloud.elastic.co/deployments).
1. [ ] Take a snapshot before upgrade:
    - Under `Elasticsearch` > `Snapshots`, click on `Take snapshot now` option.
    - Check [list of snapshots](https://gitlab-docs-website.kb.us-central1.gcp.cloud.es.io/app/management/data/snapshot_restore/snapshots)
    to confirm your latest snapshot.
1. [ ] In the `Deployment version` section, click `Upgrade`.
1. [ ] Select latest version.
1. [ ] Click `Upgrade` and then `Confirm upgrade`. The deployment should take only a few minutes to create.
1. [ ] [Test locally](#production-local-testing) to confirm.

#### Production: Local testing

1. Connect to production cluster:
    - Copy `CLOUD_ID` from production deployment's [Search homepage](https://gitlab-docs-website.kb.us-central1.gcp.cloud.es.io/app/enterprise_search/overview).
    - In the project, change `ELASTIC_CLOUD_ID` in `constants.js` file to production instance's `CLOUD_ID` copied from
    previous step.
    - Copy and paste the following [CI/CD variables](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/settings/ci_cd#js-cicd-variables-settings):
    `ELASTIC_KEY` and `ELASTIC_INDEX` in your shell configuration file.
1. Start up local environment and make sure search is still working as expected and analytics dashboard is updating
metrics data (i.e. search queries and link clicks).
1. If search and analytics functionality are no longer work, follow the [rollback procedure](#test-rollback-version) in
production deployment or open a [change request](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?description_template=change_management)
with Infrastructure team.

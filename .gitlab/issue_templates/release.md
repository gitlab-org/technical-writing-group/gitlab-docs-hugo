<!--
SET TITLE TO: docs.gitlab.com release XX.ZZ (month, YYYY)
-->

## Tasks for all releases

Documentation [for handling the docs release](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/doc/releases.md)
is available.

Prerequisites:

- Make sure you have all the [needed tools](/doc/setup.md) installed on your system.

Terminology:

The following terms are used throughout this document:

- **Stable branch**: This is the branch that matches the GitLab version being released. For example,
  for GitLab 17.2, the stable branch is `17.2`.

### On Monday the week of the release

1. [ ] Cross-link to the main MR for the release post: `<add link here>`
   ([Need help finding the MR?](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&label_name%5B%5D=release%20post&label_name%5B%5D=release))
1. [ ] In this issue, create separate **threads** for the retrospective, and add items as they appear:

   ```markdown
   ## :+1: What went well this release?
   ## :-1: What didn't go well this release?
   ## :chart_with_upwards_trend: What can we improve going forward?
   ```

1. [ ] Add the version to be removed from the version dropdown list to the [docs archives](https://gitlab.com/gitlab-org/gitlab-docs-archives) (`gitlab-docs-archives`) repository.

   1. In the `docs-gitlab-com` repository, open [`content/versions.json`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/content/versions.json#L5). Find the `last_minor` hash, and copy the _earlier_ version number.

   1. In the `gitlab-docs-archives` repository,
      [create a branch using the UI](https://gitlab.com/gitlab-org/gitlab-docs-archives/-/branches/new),
      and name it after the version you copied before.

      Do NOT create a merge request. After a couple of minutes, the version will be
      deployed. You can visit `https://archives.docs.gitlab.com/<version>` and
      verify.

1. [Create a stable branch and Docker image for the release](#create-a-stable-branch-and-docker-image-for-the-release).

#### Create a stable branch and Docker image for the release

1. [ ] In the root path of the `docs-gitlab-com` repository:

   - Update your local clone:

     ```shell
     make update
     ```

   - Install all project dependencies:

     ```shell
     make setup
     ```

1. [ ] To practice running the task and check the process, run the task in dry run mode:

   ```shell
   DRY_RUN=true make create-stable-branch VERSION={version in X.Y format}
   ```

1. [ ] Create the stable branch:

   ```shell
   DRY_RUN=false make create-stable-branch VERSION={version in X.Y format}
   ```

   - A branch `X.Y` for the release is created.
   - A new `X.Y.Dockerfile` is created and automatically committed.
   - The new branch is pushed.
   - **Do not create an MR for this step. This file is only used when a stable branch pipeline is run.**

### On the Thursday of the release, or the day after

After the release post is live, or the day after:

1. Check that the stable branches that correspond with the release are present. It's OK if the branch pipeline is failing for non-docs reasons:

   - [ ] `gitlab`: <https://gitlab.com/gitlab-org/gitlab/-/branches?state=all&sort=updated_desc&search=stable-ee>
   - [ ] `gitlab-runner`: <https://gitlab.com/gitlab-org/gitlab-runner/-/branches?state=all&sort=updated_desc&search=-stable>
   - [ ] `omnibus-gitlab`: <https://gitlab.com/gitlab-org/omnibus-gitlab/-/branches?state=all&sort=updated_desc&search=-stable>
   - [ ] `charts/gitlab`: <https://gitlab.com/gitlab-org/charts/gitlab/-/branches?state=all&sort=updated_desc&search=-stable> (Version number is 9 lower than `gitlab` release, so GitLab 17.X = Charts 8.X)

   If not, you cannot proceed to the next step, so you'll have to wait.

1. [ ] Run a new pipeline targeting the docs stable branch. When the pipeline runs, the
   [`image:docs-single` job](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/.gitlab/ci/docker-images.gitlab-ci.yml/#L68-96)
   builds a new Docker image tagged with the name of the stable branch containing
   all the versioned documentation
   (for example, see [the 17.5 release pipeline](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/pipelines/1619475939)).

   Verify that the [pipeline](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/pipelines) for the stable branch
   has passed and created a [Docker image](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/container_registry/8242887)
   tagged with the release version. ([If it fails, how do I fix it?](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/doc/releases.md#imagedocs-single-job-fails-when-creating-the-docs-stable-branch))

   To filter the list of pipelines to the stable branch, in the **Filter pipelines** text box, select the **Branch name** filter, then manually enter the current stable branch. For example, `Branch name = 17.2`.

1. Create a docs.gitlab.com release merge request, which updates the version dropdown list for all online versions and
   updates the archives list:
   1. [ ] If `GITLAB_TOKEN` is not already set in your local environment, do the following steps first:
      1. [Create a personal access token](https://gitlab.com/-/user_settings/personal_access_tokens) with the `api` scope.
      1. Set the value of this token as an environment variable called `GITLAB_TOKEN` in your shell settings file.
        - For example, if you're using Zsh on MacOS, add this to `~/.zshrc`:
          `export GITLAB_TOKEN="your-token-value-here"`.

   1. In the root path of the `docs-gitlab-com` repository:
      1. [ ] Create merge request with updated versions:

        ```bash
         make create-release-merge-request VERSION={version in X.Y format}
        ```

        - Stashes local changes and updates `main` branch.
        - Creates a branch `release-X-Y`.
        - Updates lists of versions in [`content/versions.json`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/content/versions.json).
            - Example:
                - Sets `next` to the version number of the next release. For example, if you're releasing `17.2`, sets `next` to `17.3`.
                - Sets `current` to the version number of the release you're releasing. For example, if you're releasing `17.2`, sets `current` to `17.2`.
                - Sets `last_minor` to the last two most recent minor releases. For example, if you're releasing `17.2`, sets `last_minor` to `17.1` and `17.0`.
                - Ensures `last_major` is set to the two most recent major versions.
                  For example, if you're releasing `17.2`, ensure `last_major` are `16.11` and `15.11`.

                As a complete example, the `content/versions.json` file for the `17.2` release is:

                ```json
                [
                  {
                    "next": "17.3",
                    "current": "17.2",
                    "last_minor": ["17.1", "17.0"],
                    "last_major": ["16.11", "15.11"]
                  }
                ]
                ```

        - In [`.gitlab/ci/docker-images.gitlab-ci.yml`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/.gitlab/ci/docker-images.gitlab-ci.yml),
         under the test:image:docs-single job, it changes the `GITLAB_VERSION` variable to the version number of the release
         you're releasing.
        - Commits and pushes to create the merge request (but without running any `lefthook` tests), with a `~release` label.

   1. [ ] Verify the merge request created:

      - There is a `~release` label on the MR.
      - Verify that the **Changes** tab includes the following:
        - `.gitlab/ci/docker-images.gitlab-ci.yml`
        - `content/versions.json`

1. Deploy the versions:

   1. [ ] Ask in slack in `#tw-team` for someone to review and merge the release merge request for you.
   1. [ ] Go to the [scheduled pipelines page](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/pipeline_schedules)
      and run the `Build docker images pipeline (Manual)` pipeline.
   1. [ ] Once previous pipeline is completed, run the `Build docs.gitlab.com every hour` scheduled pipeline.

1. [ ] After the deployment completes, open `docs.gitlab.com` in a browser. Confirm
   both the latest version and the correct pre-release version are listed in the version dropdown list.
1. [ ] Check the version dropdown list to ensure all versions of the docs are visible and their version dropdown lists have the expected versions.
   - Versions hosted on `docs.gitlab.com` should show the same version options as the pre-release site.
   - Versions hosted on `archives.docs.gitlab.com` should only show their own version and a link back to the archives page.
     - This applies to GitLab 15.6 and later. Earlier versions might have broken links in the version dropdown list.
       This will eventually be resolved as the earlier versions are phased out.
1. [ ] Share the following message in the `#tw-team` channel:

   ```plaintext
   :mega: The docs <version> release is complete. If you have any feedback about this release, add it to the retro thread in <this issue>.
   ```

After the docs release is complete:

1. [ ] [Create a release issue](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/issues/new?issue%5Btitle%5D=docs.gitlab.com+release+XX.ZZ+%28month%2C+YYYY%29&issuable_template=Default) for the next release, and assign it to the TW who completed the
   [release post structural check for the current milestone](https://handbook.gitlab.com/handbook/marketing/blog/release-posts/managers/).
1. [ ] **Major releases only.** Update
   [`OutdatedVersions.yml`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/.vale/gitlab_base/OutdatedVersions.yml)
   with the latest outdated version.
1. [ ] Improve [this checklist](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/.gitlab/issue_templates/release.md).
Continue moving steps from [`releases.md`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/doc/releases.md)
to here until the issue template is the single source of truth and the documentation provides extra information.

## Helpful links

- [Troubleshooting info](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/doc/releases.md#troubleshooting)
- [List of upcoming assignees for overall release post](https://handbook.gitlab.com/handbook/marketing/blog/release-posts/managers/)
- [Internal docs for handling the docs release](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/doc/releases.md)

/label ~"Technical Writing" ~"type::maintenance" ~"maintenance::refactor" ~"Category:Docs Site" ~release

## Add navigation item to global navigation

- [ ] Link to the merge request that introduced the new page:
- [ ] Give the MR a descriptive title.
- [ ] Optional. Link to the review app showing the new page:

/label ~"global nav" ~"Technical Writing" ~"type::maintenance" ~"maintenance::refactor"
/assign me

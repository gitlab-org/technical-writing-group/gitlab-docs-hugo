#!/bin/bash

# Download the latest release of the @gitlab/svgs package.
#
# Yarn fetches @gitlab/svgs as part of the frontend
# build process, but we need this separate script to fetch the
# latest release because we have different requirements
# for the two places we use this package:
#
# 1. Hugo "icon" shortcode: requires the latest SVGs.
#    Contributors expect available SVGs to include everything
#    listed on https://gitlab-org.gitlab.io/gitlab-svgs/.
# 2. GitLab UI: requires a Yarn-managed version in order to
#    ensure compatibility with our installed version of @gitlab/ui.
#
# Yarn, by design, does not allow us to grab the latest version
# on build: it will always defer to what's in yarn.lock.
# So, in this script, we fetch the latest version with Bash.

set -euo pipefail

echo "Fetching latest GitLab SVGs..."

THEME_DEST_DIR="themes/gitlab-docs/static/gitlab_ui/svgs-latest"
DATA_DEST_DIR="data"

# Ensure destination directories exist
mkdir -p "$THEME_DEST_DIR" "$DATA_DEST_DIR"

# Download and extract the npm package
echo "Downloading latest package from npm..."
if ! npm pack @gitlab/svgs@latest --silent; then
    echo "Error: Failed to download @gitlab/svgs package from npm" >&2
    exit 1
fi

# Get the downloaded filename (should be something like gitlab-svgs-3.119.0.tgz)
PACKAGE_FILE=$(ls gitlab-svgs-*.tgz)

echo "Extracting files..."
if ! tar -xzf "$PACKAGE_FILE"; then
    echo "Error: Failed to extract $PACKAGE_FILE" >&2
    exit 1
fi

# Move files to destinations
echo "Moving files to $THEME_DEST_DIR..."
if ! mv "package/dist/icons.svg" "$THEME_DEST_DIR/"; then
    echo "Error: Failed to move icons.svg to $THEME_DEST_DIR" >&2
    exit 1
fi
if ! mv "package/dist/icons.json" "$DATA_DEST_DIR/"; then
    echo "Error: Failed to move icons.json to $DATA_DEST_DIR" >&2
    exit 1
fi

# Cleanup
rm -rf package "$PACKAGE_FILE"

echo "Successfully updated SVGs"

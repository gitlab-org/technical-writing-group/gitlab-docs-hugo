#!/bin/bash

# Check docs content for invalid index filenames.
# We require that index files are named _index.md,
# not index.md.

SEARCH_DIR="$1"

# Function to check for invalid index.md files in a directory
check_index_files() {
    local dir="$1"

    if [ ! -d "$dir" ]; then
        printf "Error: Directory '%s' does not exist\n" "$dir"
        return 1
    fi

    printf "Checking index files in %s...\n" "$dir"

    local index_files
    index_files=$(find "$dir" -type f -name 'index.md')

    if [ -n "$index_files" ]; then
        printf "Error: Found invalid index.md files. Hugo requires these to be named _index.md:\n"
        printf "%s\n" "$index_files"
        return 1
    else
        printf "No invalid index.md files found in %s\n" "$dir"
        return 0
    fi
}

# If SEARCH_DIR is provided, only check that directory
if [ -n "$SEARCH_DIR" ]; then
    check_index_files "$SEARCH_DIR"
    exit $?
fi

# If no SEARCH_DIR provided, check all directories from YAML
# Ensure yq is available
if ! command -v yq >/dev/null 2>&1; then
    >&2 printf "ERROR: yq is required but not installed. Exiting.\n"
    exit 1
fi

PRODUCTS_YAML="data/products.yaml"
if [ ! -f "$PRODUCTS_YAML" ]; then
    printf "Error: Products YAML file '%s' does not exist\n" "$PRODUCTS_YAML"
    exit 1
fi

exit_code=0

# Iterate over each repository entry in the YAML file
for ENTRY in $(yq eval '.products | keys | .[]' "$PRODUCTS_YAML"); do
    CLONE_DIR=$(yq eval ".products[\"$ENTRY\"].clone_dir" "$PRODUCTS_YAML")
    DOCS_DIR=$(yq eval ".products[\"$ENTRY\"].docs_dir" "$PRODUCTS_YAML")
    DOCS_PATH="$CLONE_DIR"/"$DOCS_DIR"

    # Check each directory, but don't exit immediately on error
    if ! check_index_files "$DOCS_PATH"; then
        exit_code=1
    fi
done

exit $exit_code

#!/usr/bin/env bash

checkRedirectThreshold() {
  COLOR_RED="\e[31m"
  COLOR_YELLOW="\e[33m"
  COLOR_GREEN="\e[32m"

  # path to the file
  local file_path="public/_redirects"

  # Check if file exists
  if [ ! -f "$file_path" ]; then
      printf "%bERROR: File $file_path does not exist.%b\n" "${COLOR_RED}" "${COLOR_RESET}"
      return 1
  fi

  # Store line count in redirects file
  line_count=$(awk 'END {print NR}' "$file_path")

  # Check if total redirects are over threshold (1000)
  if [ "$line_count" -ge 1000 ]; then
    printf "%bERROR: The number of redirects ($line_count) exceeds the threshold of 1000.%b\n" "${COLOR_RED}" "${COLOR_RESET}"
    return 1
  elif [ "$line_count" -gt 900 ] && [ "$line_count" -lt 1000 ]; then
    printf "%bWARNING: The number of redirects ($line_count) almost or at threshold.%b\n" "${COLOR_YELLOW}" "${COLOR_RESET}"
    return 2
  else
    printf "%bINFO: The number of redirects ($line_count) is within the threshold.%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"
  fi
}

checkRedirectThreshold

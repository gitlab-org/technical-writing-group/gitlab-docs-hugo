#!/usr/bin/env bash
set -euo pipefail

COLOR_RED="\e[31m"
COLOR_GREEN="\e[32m"
COLOR_RESET="\e[39m"

check_analytics() {
  local file="public/index.html"
  local search_term="googletagmanager"

  if [[ ! -f "$file" ]]; then
    printf "%bERROR: %s not found. Ensure you're in the correct directory and the site has been built.%b\n" "${COLOR_RED}" "$file" "${COLOR_RESET}"
    return 1
  fi

  if [[ "${ANALYTICS_ENABLED:-false}" == "true" ]]; then
    if grep -q "$search_term" "$file"; then
      printf "%bPASS: Analytics scripts found in index.html as expected.%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"
      return 0
    else
      printf "%bFAIL: Analytics scripts not found in index.html, but were expected.%b\n" "${COLOR_RED}" "${COLOR_RESET}"
      return 1
    fi
  else
    if ! grep -q "$search_term" "$file"; then
      printf "%bPASS: No analytics scripts found in index.html, as expected.%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"
      return 0
    else
      printf "%bFAIL: Analytics scripts found in index.html, but were not expected.%b\n" "${COLOR_RED}" "${COLOR_RESET}"
      return 1
    fi
  fi
}

check_analytics

#!/usr/bin/env node

async function checkPermission({ action, description, shouldSucceed }) {
  try {
    await action();
    const result = shouldSucceed ? "PASS" : "FAIL";
    console.info(`- ${result}: ${description}`);
    return shouldSucceed;
    // eslint-disable-next-line no-unused-vars
  } catch (error) {
    const result = shouldSucceed ? "FAIL" : "PASS";
    console.info(`- ${result}: ${description}`);
    return !shouldSucceed;
  }
}

async function main() {
  try {
    const { createSearchClient, testSearchQuery, INDEX_NAME } = await import(
      // eslint-disable-next-line import/extensions
      "../__tests__/helpers/jest_helpers.js"
    );

    // Create search client.
    const client = await createSearchClient();

    const checks = [
      checkPermission({
        action: async () =>
          testSearchQuery(client, { query: { match_all: {} }, size: 0 }),
        description: "Search successful",
        shouldSucceed: true,
      }),
      checkPermission({
        action: async () =>
          client.create({
            index: INDEX_NAME,
            id: `permission_check_${Date.now()}_${Math.random().toString(36).substring(2, 15)}`,
            body: { test: "This should fail" },
            op_type: "create",
          }),
        description: "Key cannot write to index",
        shouldSucceed: false,
      }),
      checkPermission({
        action: async () => client.cluster.health(),
        description: "Key cannot access cluster health",
        shouldSucceed: false,
      }),
    ];

    const results = await Promise.all(checks);

    if (results.every(Boolean)) {
      console.info("\nSUCCESS: API key has the correct permissions.");
      process.exit(0);
    } else {
      console.error("\nERROR: API key does not have the correct permissions.");
      process.exit(1);
    }
  } catch (error) {
    console.error("ERROR:", error.message);
    process.exit(1);
  }
}

main();

#!/usr/bin/env bash
set -euo pipefail

COLOR_RED="\e[31m"
COLOR_GREEN="\e[32m"
COLOR_RESET="\e[39m"

NAVIGATION_FILE="data/navigation.yaml"

check_duplicates() {
    local duplicates
    duplicates=$(sed -n -E "s/^[[:space:]]*url: '(.*)'/\1/p" "${NAVIGATION_FILE}" | sort | uniq -d)

    printf "%bINFO: Checking for duplicate global navigation entries...%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"

    if [[ -n $duplicates ]]; then
        while IFS= read -r entry; do
            printf "%bERROR (duplicate entry):%b %s\n" "${COLOR_RED}" "${COLOR_RESET}" "${entry}"
        done <<< "$duplicates"
        return 1
    else
        printf "%bINFO: No duplicate entries found!%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"
        return 0
    fi
}

check_index_html() {
    local index_entries
    index_entries=$(sed -n -E "s/^[[:space:]]*url: '(.*)'/\1/p" "${NAVIGATION_FILE}" | grep -e "index.html" | grep -v "index.html.")

    printf "%bINFO: Checking for global navigation entries with index.html...%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"

    if [[ -n $index_entries ]]; then
        while IFS= read -r entry; do
            printf "%bERROR (entry with index.html):%b %s\n" "${COLOR_RED}" "${COLOR_RESET}" "${entry}"
        done <<< "$index_entries"
        return 1
    else
        printf "%bINFO: No entries with index.html found!%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"
        return 0
    fi
}

check_absolute_paths() {
    local absolute_paths
    absolute_paths=$(sed -n -E "s/^[[:space:]]*url: '(\/.*)'/\1/p" "${NAVIGATION_FILE}")

    printf "%bINFO: Checking for global navigation entries with absolute paths...%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"

    if [[ -n $absolute_paths ]]; then
        while IFS= read -r entry; do
            printf "%bERROR (entry with absolute path):%b %s\n" "${COLOR_RED}" "${COLOR_RESET}" "${entry}"
        done <<< "$absolute_paths"
        return 1
    else
        printf "%bINFO: No entries with absolute paths found!%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"
        return 0
    fi
}

check_schema() {
    printf "${COLOR_GREEN}INFO: Checking global navigation against schema...${COLOR_RESET}\n"

    if node __tests__/navigation/validate_schema.js; then
        return 0
    else
        return 1
    fi
}

check_links() {
    printf "%bINFO: Checking global navigation links...%b\n" "${COLOR_GREEN}" "${COLOR_RESET}"

    if DEBUG="${DEBUG:-}" node __tests__/navigation/check_menu_links.cjs; then
        return 0
    else
        return 1
    fi

}

main() {
    local return_code=0

    check_duplicates || return_code=1
    check_index_html || return_code=1
    check_absolute_paths || return_code=1
    check_schema || return_code=1
    check_links || return_code=1

    return "$return_code"
}

main "$@"

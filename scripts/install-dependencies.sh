#!/usr/bin/env bash

COLOR_RED="\e[31m"
COLOR_YELLOW="\e[33m"
COLOR_GREEN="\e[32m"
COLOR_RESET="\e[39m"

if [ ! -f .tool-versions ]; then
  printf "${COLOR_RED}ERROR: .tool-versions file not found!${COLOR_RESET}\n"
  exit 1
fi

if command -v mise &> /dev/null; then
    printf "${COLOR_GREEN}INFO: mise found! Checking for mise updates...${COLOR_RESET}\n"
    if error_output=$(mise self-update --yes 2>&1); then
      printf "${COLOR_GREEN}INFO: mise update complete!${COLOR_RESET}\n"
    else
      printf "${COLOR_RED}ERROR: mise self-update returned an error:${COLOR_RESET}\n$error_output.\n"
      printf "${COLOR_GREEN}INFO: If you installed mise by using a package manager, the error is expected.${COLOR_RESET}\n"
    fi
    printf "${COLOR_GREEN}INFO: Using mise to install dependencies...${COLOR_RESET}\n"
    mise install
    corepack enable
elif command -v asdf &> /dev/null; then
    printf "${COLOR_YELLOW}WARNING: asdf found but not supported for installing dependencies! For more information, see doc/setup.md.${COLOR_RESET}\n"
else
  printf "${COLOR_YELLOW}WARNING: asdf and mise not found! For more information, see doc/setup.md.${COLOR_RESET}\n"
fi

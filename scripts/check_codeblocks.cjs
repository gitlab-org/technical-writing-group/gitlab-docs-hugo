#!/usr/bin/env node

/**
 * @file check_codeblocks.js
 * Checks for code blocks in gitlab document directories for any unsupported languages.
 */

const fs = require("fs");
const path = require("path");

// Path to the supported languages file.
// Need this so that the script can find the location of this file correctly.
const SUPPORTED_LANGUAGES_FILE = path.join(
  __dirname,
  "supported-code-languages.json",
);

// List of document directories to check.
const DOCS_DIRS = [
  "../gitlab/doc",
  "../gitlab-operator/doc",
  "../gitlab-runner/docs",
  "../charts-gitlab/doc",
  "../omnibus-gitlab/doc",
];

// Get supported languages from the file.
function getSupportedLanguages() {
  try {
    const content = fs.readFileSync(SUPPORTED_LANGUAGES_FILE, "utf-8");
    const languageMappings = JSON.parse(content);

    // Create a set of all the language keys (which are lowercase)
    return new Set(Object.keys(languageMappings));
  } catch (error) {
    console.error(`Error reading or parsing the language file: ${error}`);
    return new Set();
  }
}

// Extract the language from a line containing a code block.
function extractLanguage(line) {
  const cbLanguage = line.match(/^`{3,}[ \t]*(\S*)/);
  return cbLanguage ? cbLanguage[1].toLowerCase() : "";
}

// Process a Markdown file to find the top-level code block language.
function checkFile(filePath, supportedLanguages) {
  const content = fs.readFileSync(filePath, "utf-8");
  const lines = content.split("\n");
  // inCodeBlock is to help keep track of main code block when there's nested code blocks.
  let inCodeBlock = false;
  let mainLanguage = "";
  let lineNum = 0;

  for (const line of lines) {
    lineNum += 1;
    if (line.startsWith("```")) {
      if (!inCodeBlock) {
        // Start of a main code block.
        mainLanguage = extractLanguage(line);
        inCodeBlock = true;
        if (
          mainLanguage !== "" &&
          !supportedLanguages.has(mainLanguage.toLowerCase())
        ) {
          const filePathWithLineNum = `${filePath}:${lineNum}`;
          console.info(
            `The language '${mainLanguage}' in file ${filePathWithLineNum} is not supported.`,
          );
        }
      } else {
        // End of the main code block.
        inCodeBlock = false;
      }
    }
  }
}

// Main function to process files in a directory
// Recursively process all directories and files
function processDirectory(dirPath, supportedLanguages) {
  fs.readdir(dirPath, (error, files) => {
    if (error) {
      console.error(`Error reading directory ${dirPath}: ${error}`);
      return;
    }
    files.forEach((file) => {
      const filePath = path.join(dirPath, file);
      fs.stat(filePath, (fileErr, stats) => {
        if (fileErr) {
          console.error(
            `Error getting file stats for file ${filePath}: ${fileErr}`,
          );
          return;
        }
        // Process subdirectories.
        if (stats.isDirectory()) {
          processDirectory(filePath, supportedLanguages);
        } else if (
          stats.isFile() &&
          (file.endsWith(".md") || file.endsWith(".html"))
        ) {
          // Check Markdown/HTML files for unsupported languages.
          checkFile(filePath, supportedLanguages);
        }
      });
    });
  });
}

// Get supported languages.
const supportedLanguages = getSupportedLanguages();
// Process each directory.
DOCS_DIRS.forEach((dir) => {
  processDirectory(dir, supportedLanguages);
});

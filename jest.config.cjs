const VUE_JEST_TRANSFORMER = "@vue/vue2-jest";

module.exports = {
  testMatch: ["<rootDir>/__tests__/**/*_spec.js"],
  moduleFileExtensions: ["js", "json", "vue"],
  cacheDirectory: "<rootDir>/tmp/cache/jest",
  restoreMocks: true,
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.vue$": VUE_JEST_TRANSFORMER,
    "^.+\\.svg$": VUE_JEST_TRANSFORMER,
  },
  transformIgnorePatterns: [
    "node_modules/(?!(@gitlab/(ui|svgs)|bootstrap-vue)/)",
  ],
  setupFilesAfterEnv: ["<rootDir>/__tests__/jest.overrides.js"],
};

/**
 * Override warning output to ignore deprecation warnings for punycode for now.
 * We have a transitive dependency via jsdom that still uses the deprecated package.
 */
process.removeAllListeners("warning");
process.on("warning", (warning) => {
  // https://nodejs.org/api/deprecations.html#DEP0040
  if (warning.name === "DeprecationWarning" && warning.code === "DEP0040") {
    return;
  }
  console.warn(warning);
});

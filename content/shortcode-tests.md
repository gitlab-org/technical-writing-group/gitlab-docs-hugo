---
title: Shortcode test cases
noindex: true
draft: true
---

## Tabs with an image inside

{{< tabs >}}

{{< tab title="Tab one" >}}

![Just a placeholder](img/200x200.svg)

{{< /tab >}}

{{< tab title="Tab two" >}}

No images on this tab.

{{< /tab >}}

{{< /tabs >}}

## Tabs in a list

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

- This is a list

  {{< tabs >}}

  {{< tab title="Tab one" >}}

  Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
  ullamco laboris nisi ut aliquip ex ea commodo consequat.

  {{< /tab >}}

  {{< tab title="Tab two" >}}

  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
  occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

  {{< alert type="note" >}}

  Lorem ipsum dolor sit amet, consectetur adipiscing elit.

  {{< /alert >}}

  {{< /tab >}}

  {{< /tabs >}}

- List item two
- List item three

## Tabs nested within a list of a list

- This is a list
  - This is a nested list
    {{< tabs >}}

    {{< tab title="Tab one" >}}

    Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
    ullamco laboris nisi ut aliquip ex ea commodo consequat.

    {{< /tab >}}

    {{< tab title="Tab two" >}}

    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

    {{< /tab >}}

    {{< /tabs >}}

- List item two
- List item three

## Tabs nested within a list of a list and has tab with a listed alert

- This is a list
  - This is a nested list
    {{< tabs >}}

    {{< tab title="Tab one" >}}

    Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
    ullamco laboris nisi ut aliquip ex ea commodo consequat.

    {{< /tab >}}

    {{< tab title="Tab two" >}}

    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

    {{< alert type="note" >}}

      Here's some note text.

      You can add any standard markdown inside any of
      these alerts. For example, here's a list:

        - One
        - Two
        - Five hundred

    {{< /alert >}}

    {{< /tab >}}

    {{< /tabs >}}

- List item two
- List item three

## Alert inside of a tab

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{{< tabs >}}

{{< tab title="Tab one" >}}

  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
  occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{{< alert type="note" >}}

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{{< /alert >}}

{{< /tab >}}

{{< tab title="Tab two" >}}

  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
  occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{{< /tab >}}

{{< /tabs >}}

## Icon on a list item

- Lorem ipsum 1 {{< icon name="check-circle" >}}.
  - Lorem ipsum 1.1 {{< icon name="check-circle" >}}.
- Lorem ipsum 2.

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

## Icon inside an alert

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{{< alert type="note" >}}

Lorem ipsum dolor sit amet, consectetur adipiscing elit. {{< icon name="check-circle" >}}

{{< /alert >}}

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

## Icon inside a tab

- This is a list

  {{< tabs >}}

  {{< tab title="Tab one" >}}

  Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
  ullamco laboris nisi ut aliquip ex ea commodo consequat.

  {{< /tab >}}

  {{< tab title="Tab two" >}}

  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
  occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

  {{< icon name="check-circle" >}}

  Sed do eiusmod tempor incididunt.

  {{< /tab >}}

  {{< /tabs >}}

## History with normal markup

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{{< history >}}

- [Sed do](https://test.com) Sed do eiusmod tempor incididunt. Disabled by default.
- Sed do eiusmod tempor incididunt.

{{< /history >}}

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

## History within a list item

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.

  {{< history >}}

  - [Sed do](https://test.com) Sed do eiusmod tempor incididunt. Disabled by default.
  - Sed do eiusmod tempor incididunt.

  {{< /history >}}

## History within a nested list item

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - Lorem ipsum dolor sit amet, consectetur adipiscing elit.

    {{< history >}}

    - [Sed do](https://test.com) Sed do eiusmod tempor incididunt. Disabled by default.
    - Sed do eiusmod tempor incididunt.

    {{< /history >}}

## Details with normal markup

Lorem ipsum dolor sit amet, consectetur adipiscing elit.

{{< details >}}

- Lorem: Lorem ipsum dolor sit amet
- Dolor: Lorem ipsum dolor sit amet

{{< /details >}}

## Details within a list item

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.

  {{< details >}}

  - Lorem: Lorem ipsum dolor sit amet
  - Dolor: Lorem ipsum dolor sit amet

  {{< /details >}}

## Details within a nested list item

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - Lorem ipsum dolor sit amet, consectetur adipiscing elit.

    {{< details >}}

    - Lorem: Lorem ipsum dolor sit amet
    - Dolor: Lorem ipsum dolor sit amet

    {{< /details >}}

## Icon rendered in a bullet point within a tab

Some text here.

{{< tabs >}}

{{< tab title="Tab one" >}}

1. Test sentence one.
1. Test sentence two:
    1. Lorem: Lorem ipsum dolor sit amet.

    1. Lorem: Lorem ipsum dolor sit amet, icon settings ({{< icon name="settings" >}}).

    1. Lorem: Lorem ipsum dolor sit amet.

    1. Lorem: Lorem ipsum dolor sit amet.

1. Test sentence three.
1. Test sentence four.

{{< /tab >}}

{{< tab title="Tab two" >}}

  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
  occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

{{< /tab >}}

{{< /tabs >}}

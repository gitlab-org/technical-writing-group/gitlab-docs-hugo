/**
 * @jest-environment jsdom
 */

import { compareVersions } from "compare-versions";
import { addSortDirection } from "../../themes/gitlab-docs/src/utils/tables/sortable";

// Mock compareVersions
jest.mock("compare-versions");

describe("Table sorting", () => {
  let table;
  // Set up a mock table structure before each test
  beforeEach(() => {
    document.body.innerHTML = `
      <table>
        <thead>
          <tr>
            <th class="sortable" data-sort="string">Name</th>
            <th class="sortable" data-sort="number" data-default="desc">Version</th>
          </tr>
        </thead>
        <tbody>
          <tr><td>Item B</td><td>16.11</td></tr>
          <tr><td>Item C</td><td>Not defined</td></tr>
          <tr><td>Item A</td><td>16.1</td></tr>
        </tbody>
      </table>
    `;
    table = document.querySelector("table");
    addSortDirection(table);
  });

  it("Sorting classes are appropriately changed based on clicks and current class", () => {
    const header = table.querySelectorAll("th.sortable")[0];
    // First click - ascending order.
    header.click();
    expect(header.classList.contains("sorted-asc")).toBe(true);
    // Second click - descending order.
    header.click();
    expect(header.classList.contains("sorted-desc")).toBeTruthy();
    // Third click - back to ascending.
    header.click();
    expect(header.classList.contains("sorted-asc")).toBeTruthy();
  });

  it("Rows are successfully sorted - strings", () => {
    const header = table.querySelector('th.sortable[data-sort="string"]');
    header.click();
    const rows = table.querySelectorAll("tbody tr");
    expect(rows[0].children[0].textContent).toBe("Item A");
    expect(rows[1].children[0].textContent).toBe("Item B");
    expect(rows[2].children[0].textContent).toBe("Item C");
  });

  it("By default, rows are sorted in descending for header with data-default", () => {
    compareVersions.mockImplementation((a, b) => {
      if (a === "Not defined" || b === "Not defined") return 0;
      return parseFloat(a) - parseFloat(b);
    });

    const defaultRows = table.querySelectorAll("tbody tr");
    expect(defaultRows[0].children[1].textContent).toBe("16.11");
    expect(defaultRows[1].children[1].textContent).toBe("16.1");
    expect(defaultRows[2].children[1].textContent).toBe("Not defined");
  });

  it("Rows are successfully sorted - numbers/floats", () => {
    compareVersions.mockImplementation((a, b) => {
      if (a === "Not defined" || b === "Not defined") return 0;
      return parseFloat(a) - parseFloat(b);
    });

    const header = table.querySelector('th.sortable[data-sort="number"]');
    // Ascending order
    header.click();
    const ascRows = table.querySelectorAll("tbody tr");
    expect(ascRows[0].children[1].textContent).toBe("16.1");
    expect(ascRows[1].children[1].textContent).toBe("16.11");
    expect(ascRows[2].children[1].textContent).toBe("Not defined");

    // Descending order
    header.click();
    const descRows = table.querySelectorAll("tbody tr");
    expect(descRows[0].children[1].textContent).toBe("16.11");
    expect(descRows[1].children[1].textContent).toBe("16.1");
    expect(descRows[2].children[1].textContent).toBe("Not defined");
  });
});

// Mocks https://docs.gitlab.com/versions.json
export const mockVersions = {
  next: "17.1",
  current: "17.0",
  last_minor: ["16.11", "16.10"],
  last_major: ["16.11", "15.11"],
};

// Mocks https://archives.docs.gitlab.com/archive_versions.json
export const mockArchiveVersions = [
  "15.0",
  "15.1",
  "15.2",
  "15.11",
  "16.0",
  "16.9",
];

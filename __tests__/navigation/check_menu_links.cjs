#!/usr/bin/env node

/**
 * Checks that links in navigation.yaml point to valid pages on the live site.
 */

const fs = require("fs");
const yaml = require("js-yaml");
const { XMLParser } = require("fast-xml-parser");

const NAVIGATION_FILE = "data/navigation.yaml";
const BASE_URL = "https://docs.gitlab.com/";
const SITEMAP_URL = `${BASE_URL}sitemap.xml`;

// ANSI color codes
const RED = "\x1b[31m";
const GREEN = "\x1b[32m";
const RESET = "\x1b[0m";

const getAllUrls = (obj) => {
  const internalUrls = new Set();
  const externalUrls = new Set();

  const extract = (o) => {
    if (o.url) {
      if (o.url.startsWith("http")) {
        externalUrls.add(o.url);
      } else {
        const baseUrl = o.url.split("#")[0];
        internalUrls.add(baseUrl);
      }
    }
    if (o.submenu) {
      o.submenu.forEach((item) => extract(item));
    }
  };

  obj.forEach((item) => extract(item));
  return { internalUrls, externalUrls };
};

const checkExternalUrl = async (url) => {
  try {
    const response = await fetch(url, { method: "HEAD" });
    return {
      url,
      status: response.status,
      ok: response.ok,
    };
  } catch (error) {
    return {
      url,
      status: 0,
      ok: false,
      error: error.message,
    };
  }
};

const checkExternalUrls = async (urls) => {
  const results = await Promise.all(
    Array.from(urls).map((url) => checkExternalUrl(url)),
  );

  return results.filter((result) => !result.ok);
};

const fetchSitemap = async () => {
  try {
    const response = await fetch(SITEMAP_URL);
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }
    const sitemapText = await response.text();
    const parser = new XMLParser();
    return parser.parse(sitemapText);
  } catch (error) {
    console.error(
      `${RED}ERROR: Failed to fetch sitemap from ${SITEMAP_URL}: ${error.message}${RESET}`,
    );
    return null;
  }
};

const main = async () => {
  // Parse navigation YAML
  const navigation = yaml.load(fs.readFileSync(NAVIGATION_FILE, "utf8"));
  const { internalUrls, externalUrls } = getAllUrls(navigation);

  // Fetch and parse sitemap XML
  const sitemap = await fetchSitemap();
  if (sitemap === null) {
    process.exit(1);
  }

  // Extract and normalize sitemap URLs
  const sitemapUrls = new Set(
    sitemap.urlset.url.map((u) => u.loc.replace(BASE_URL, "")),
  );

  let hasErrors = false;

  // Check internal URLs
  for (const url of internalUrls) {
    if (!sitemapUrls.has(url)) {
      console.error(
        `${RED}ERROR: No sitemap entry found for global navigation entry URL:${RESET} ${url}.\nIs the navigation entry valid and has the page been published yet?`,
      );
      hasErrors = true;
    }
  }

  // Check external URLs
  if (externalUrls.size > 0) {
    const brokenLinks = await checkExternalUrls(externalUrls);
    if (brokenLinks.length > 0) {
      hasErrors = true;
      brokenLinks.forEach((result) => {
        console.error(
          `${RED}ERROR: External URL check failed:${RESET} ${result.url} ` +
            `(Status: ${result.status}${result.error ? `, Error: ${result.error}` : ""})`,
        );
      });
    }
  }

  if (!hasErrors) {
    console.info(`${GREEN}INFO: No broken links found!${RESET}`);
  }

  if (process.env.DEBUG) {
    const debug = {
      internalUrls: Array.from(internalUrls),
      externalUrls: Array.from(externalUrls),
      sitemapUrls: Array.from(sitemapUrls),
    };
    fs.writeFileSync(
      "navigation-check-debug.log",
      JSON.stringify(debug, null, 2),
    );
    console.info("\nDebug output written to: navigation-check-debug.log");
  }

  process.exit(hasErrors ? 1 : 0);
};

main().catch((error) => {
  console.error(`${RED}ERROR: Unexpected error:${RESET}`, error);
  process.exit(1);
});

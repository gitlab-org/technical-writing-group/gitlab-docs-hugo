export const navigationSchema = {
  $schema: "http://json-schema.org/draft-07/schema#",
  description: "Global navigation for GitLab Docs",
  type: "array",
  items: {
    $ref: "#/definitions/menuItem",
  },
  definitions: {
    menuItem: {
      type: "object",
      required: ["title", "url"],
      properties: {
        title: {
          type: "string",
        },
        url: {
          type: "string",
        },
        submenu: {
          type: "array",
          items: {
            $ref: "#/definitions/menuItem",
          },
        },
      },
      additionalProperties: false,
    },
  },
};

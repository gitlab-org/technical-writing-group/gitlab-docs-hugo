/**
 * Validates the data/navigation.yaml against
 * our JSON schema.
 *
 * This ensures the YAML file uses the correct
 * stucture and data types.
 */

import fs from "fs/promises";
import path from "path";
import Ajv from "ajv";
import addFormats from "ajv-formats";
import yaml from "js-yaml";

// eslint-disable-next-line import/extensions
import { navigationSchema } from "./navigation_schema.js";

// Get the project root directory
const projectRoot = process.cwd();

const ajv = new Ajv({ allErrors: true });
addFormats(ajv);

// Define paths relative to project root
const YAML_PATH = path.join(projectRoot, "data", "navigation.yaml");

async function validateSchema() {
  try {
    const yamlContent = await Promise.all([fs.readFile(YAML_PATH, "utf8")]);

    const data = yaml.load(yamlContent, { schema: yaml.FAILSAFE_SCHEMA });
    const validate = ajv.compile(navigationSchema);
    const valid = validate(data);

    if (valid) {
      console.info("\x1b[32mINFO: Global navigation matches schema!\x1b[0m");
      return true;
    }

    console.error(
      "\x1b[31mERROR: Global navigation doesn't match schema\x1b[0m",
    );
    console.info(validate.errors);
    return false;
  } catch (error) {
    console.error(
      "\x1b[31mERROR: An error occurred during schema validation\x1b[0m",
    );
    console.error(error);
    return false;
  }
}

validateSchema()
  .then((isValid) => process.exit(isValid ? 0 : 1))
  .catch((error) => {
    console.error("Unhandled exception, please try again. Error: ", error);
    process.exit(1);
  });

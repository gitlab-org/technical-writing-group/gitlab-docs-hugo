/**
 * @jest-environment jsdom
 */
import { shallowMount } from "@vue/test-utils";
import DocsBadge from "../../themes/gitlab-docs/src/components/docs_badge.vue";
import { createContainer } from "../helpers/jest_helpers";

describe("component: Docs Badge", () => {
  it("contribute badge is rendered", () => {
    const contributeProps = {
      badgeData: {
        text: "contribute",
        hoverText: "This content is about development of the GitLab product.",
      },
    };
    const wrapper = shallowMount(DocsBadge, {
      attachTo: createContainer(),
      propsData: contributeProps,
    });
    const badgeSpan = wrapper.find(".docs-badge");
    expect(badgeSpan.exists()).toBe(true);
    expect(badgeSpan.text()).toEqual("contribute");
  });

  it("solutions badge is rendered", () => {
    const solutionsProps = {
      badgeData: {
        text: "solutions",
        hoverText: "This content is provided by GitLab Solution Architects.",
      },
    };
    const wrapper = shallowMount(DocsBadge, {
      attachTo: createContainer(),
      propsData: solutionsProps,
    });
    const badgeSpan = wrapper.find(".docs-badge");
    expect(badgeSpan.exists()).toBe(true);
    expect(badgeSpan.text()).toEqual("solutions");
  });
});

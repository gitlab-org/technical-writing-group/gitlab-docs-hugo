/**
 * @jest-environment jsdom
 */

import { shallowMount } from "@vue/test-utils";
import DocsBanner from "../../themes/gitlab-docs/src/components/docs_banner.vue";

describe("component: Survey banner", () => {
  const propsData = {
    bannerType: "survey",
    text: "Some text",
    variant: "info",
    dismissible: true,
  };
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(DocsBanner, { propsData });
  });

  it("renders a banner", () => {
    expect(wrapper.exists(".banner")).toBe(true);
  });

  it("renders the correct banner text", () => {
    const bannerText = wrapper.find("div");
    expect(bannerText.text()).toEqual(propsData.text);
  });

  it("survey banner text specific classes", () => {
    const banner = wrapper.find("div");
    expect(banner.classes()).toEqual([
      "gl-layout-w-limited",
      "gl-m-auto",
      "gl-leading-24",
    ]);
  });
});

describe("component: Archive banner", () => {
  const propsData = {
    bannerType: "archive",
    text: `This is <a href="https://docs.gitlab.com/archives">archived documentation</a> for GitLab. Go to <a href="latestURL.com">the latest</a>.`,
    variant: "tip",
    dismissible: false,
  };
  let wrapper;

  beforeEach(() => {
    wrapper = shallowMount(DocsBanner, { propsData });
  });

  it("renders a banner", () => {
    expect(wrapper.exists(".banner")).toBe(true);
  });

  it("renders the correct banner text", () => {
    const bannerText = wrapper.find("div");
    expect(bannerText.text()).toEqual(
      "This is archived documentation for GitLab. Go to the latest.",
    );
  });

  it("archive banner text specific classes", () => {
    const banner = wrapper.find("div");
    expect(banner.classes()).toEqual([
      "gl-layout-w-limited",
      "gl-m-auto",
      "gl-leading-none",
    ]);
  });
});

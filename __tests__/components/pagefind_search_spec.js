/**
 * @jest-environment jsdom
 */

import { shallowMount } from "@vue/test-utils";
import SearchPage from "../../themes/gitlab-docs/src/components/search/pagefind_search_results.vue";
import { createContainer, setMetatag } from "../helpers/jest_helpers";

describe("themes/gitlab-docs/src/components/search/pagefind_search_results.vue", () => {
  const mockTag = "gitlab_docs_version";
  const mockVersion = "16.2";

  beforeEach(() => {
    global.PagefindUI = jest.fn().mockImplementation(() => ({
      triggerSearch: jest.fn(),
    }));

    setMetatag(mockTag, mockVersion);
  });

  afterEach(() => {
    document.querySelector(`meta[name="${mockTag}"]`).remove();
  });

  it("Search form renders", async () => {
    const wrapper = shallowMount(SearchPage, {
      attachTo: createContainer(),
      data() {
        return {
          pagefind: jest.fn(),
          results: [],
          contentMap: [],
          submitted: true,
          error: false,
        };
      },
    });

    expect(wrapper.findComponent(SearchPage).isVisible()).toBe(true);
    const version = await wrapper.find('[data-testid="version-header"]').text();
    expect(version).toContain(mockVersion);
  });

  it("Pagefind initialization failed", async () => {
    const wrapper = shallowMount(SearchPage, {
      attachTo: createContainer(),
      data() {
        return {
          pagefind: jest.fn(),
          error: true,
        };
      },
    });

    expect(wrapper.find('[data-testid="pagefind-error"]').isVisible()).toBe(
      true,
    );
  });
});

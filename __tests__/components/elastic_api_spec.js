/**
 * @jest-environment node
 */
import { testSearchQuery, createSearchClient } from "../helpers/jest_helpers";
import {
  buildSearchQueryConfig,
  buildSearchQuery,
} from "../../themes/gitlab-docs/src/search/utils";

// Only run search tests if Elastic is configured in this environment.
const isSearchConfigured = () => {
  return process.env.ELASTIC_KEY !== undefined;
};
const describeIfSearchConfigured = isSearchConfigured()
  ? describe
  : describe.skip;

describeIfSearchConfigured("elastic_api", () => {
  // Create search client before running any test.
  let client;
  beforeAll(async () => {
    client = await createSearchClient();
  });

  it("Normal query returns results", async () => {
    const requestBody = {};
    const requestState = {
      searchTerm: "gitlab",
    };
    const queryConfig = buildSearchQueryConfig([]);
    const query = buildSearchQuery(requestBody, requestState, queryConfig);
    const resp = await testSearchQuery(client, query);
    expect(resp?.hits?.total?.value).toBeGreaterThan(0);
  });

  it("Normal phrase query returns results", async () => {
    const requestBody = {};
    const requestState = {
      searchTerm: "gitlab docs",
    };

    const queryConfig = buildSearchQueryConfig([]);
    const query = buildSearchQuery(requestBody, requestState, queryConfig);
    const resp = await testSearchQuery(client, query);
    expect(resp?.hits?.total?.value).toBeGreaterThan(0);
  });

  it('Query with one filter (filter: "tutorials") returns results', async () => {
    const requestBody = {};
    const requestState = {
      searchTerm: "gitlab",
    };

    const queryConfig = buildSearchQueryConfig(["tutorials"]);
    const query = buildSearchQuery(requestBody, requestState, queryConfig);
    const resp = await testSearchQuery(client, query);
    expect(resp?.hits?.total?.value).toBeGreaterThan(0);
  });

  it('Query with multiple filters (filters: ["tutorials", "subscribe", "use_gitlab", "gitlab_duo"]) returns results', async () => {
    const requestBody = {};
    const requestState = {
      searchTerm: "gitlab",
    };

    const queryConfig = buildSearchQueryConfig([
      "tutorials",
      "subscribe",
      "use_gitlab",
      "gitlab_duo",
    ]);
    const query = buildSearchQuery(requestBody, requestState, queryConfig);
    const resp = await testSearchQuery(client, query);
    expect(resp?.hits?.total?.value).toBeGreaterThan(0);
  });

  it("One letter query returns results", async () => {
    const requestBody = {};
    const requestState = {
      searchTerm: "q",
    };

    const queryConfig = buildSearchQueryConfig([]);
    const query = buildSearchQuery(requestBody, requestState, queryConfig);
    const resp = await testSearchQuery(client, query);
    expect(resp?.hits?.total?.value).toBeGreaterThan(0);
  });
});

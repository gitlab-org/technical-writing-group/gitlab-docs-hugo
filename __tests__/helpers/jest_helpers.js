/**
 * Constants.
 */
export const INDEX_NAME = "search-gitlab-docs-hugo";

/**
 * Creates a mock browser window object with a given path and/or href.
 *
 * @param {Object} options
 *   Can include "href" or "pathname" properties
 */
export const setWindowLocation = (options = {}) => {
  const { pathname, href } = options;
  const location = {
    ...window.location,
    ...(pathname && { pathname }),
    ...(href && { href }),
  };
  Object.defineProperty(window, "location", {
    writable: true,
    value: location,
  });
};

/**
 * Creates a mock metatag.
 *
 * @param {String} tagName
 * @param {String} tagContent
 */
export const setMetatag = (tagName, tagContent) => {
  const meta = document.createElement("meta");
  meta.setAttribute("name", tagName);
  meta.setAttribute("content", tagContent);
  document.head.appendChild(meta);
};

/**
 * Creates a generic container element.
 *
 * @param {*} tag
 */
export const createContainer = (tag = "div") => {
  const container = document.createElement(tag);
  document.body.appendChild(container);
  return container;
};

/**
 * Does a search on Elasticsearch API instance using provided params.
 * @param {Client} client
 * @param {Object} query
 */
export const testSearchQuery = async (client, query) => {
  try {
    const response = await client.search({
      index: INDEX_NAME,
      body: query,
    });
    return response;
    // eslint-disable-next-line no-unused-vars
  } catch (err) {
    return {};
  }
};

/**
 * Creates and returns Elasticsearch API client.
 */
export const createSearchClient = async () => {
  const { Client } = await import("@elastic/elasticsearch");
  const { ELASTIC_CLOUD_ID } = await import(
    // eslint-disable-next-line import/extensions
    "../../themes/gitlab-docs/src/search/constants.js"
  );

  if (!ELASTIC_CLOUD_ID || !process.env.ELASTIC_KEY) {
    throw new Error("Missing ELASTIC_CLOUD_ID or ELASTIC_KEY");
  }

  const client = new Client({
    cloud: { id: ELASTIC_CLOUD_ID },
    auth: { apiKey: process.env.ELASTIC_KEY },
  });

  return client;
};

# Shortcodes

[Shortcodes](https://gohugo.io/content-management/shortcodes/)
are snippets of template code that we can include in our Markdown
content to display non-standard elements on a page, such as alert
boxes or tabs. Shortcodes are the Hugo equivalent of
[Filters](https://nanoc.app/doc/filters/) in Nanoc.

This page is intended to be a development reference for all supported shortcodes.

When you want to document literal shortcode tags, you have to escape them:

```text
{{</* alert type="note" */>}}

{{</* /alert */>}}
```

## Alert boxes

[Alert boxes](https://docs.gitlab.com/development/documentation/styleguide/#alert-boxes)

### Note

```text
{{< alert type="note" >}}

Here's some note text.

You can add any standard markdown inside any of
these alerts. For example, here's a list:

- One
- Two
- Five hundred

{{< /alert >}}
```

### Warning

```text
{{< alert type="warning" >}}

This is a warning!

{{< /alert >}}
```

### Flag

```text
{{< alert type="flag" >}}

This is a feature flag.

{{< /alert >}}
```

### Disclaimer

```text
{{< alert type="disclaimer" />}}
```

## Availability details

[Availability details](https://docs.gitlab.com/development/documentation/styleguide/availability_details/)

```text
{{< details >}}

- Tier: Premium, Ultimate with [GitLab Duo Pro](https://docs.gitlab.com/subscriptions/subscription-add-ons/)
- Offering: GitLab.com, GitLab Self-Managed, GitLab Dedicated
- Status: Experiment
- Hotdogs: Yes
- Some other thing: Absolutely not

{{< /details >}}
```

## Version history

[Version history](https://docs.gitlab.com/development/documentation/styleguide/availability_details/#history)

```text
{{< history >}}

- Introduced in [GitLab 17.0](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/)
- Deprecated in GitLab 17.1

{{< /history >}}
```

### Availability details with version history

```text
{{< details >}}

- Tier: some tiers
- Offering: some offerings
- Status: Experiment
- Hotdogs: Yes

{{< /details >}}

{{< history >}}

- Introduced in xyz
- Deprecated in abc

{{< /history >}}
```

## Icons

[Icons](https://docs.gitlab.com/development/documentation/styleguide/#gitlab-svg-icons)

```text
This is an icon: {{< icon name="tanuki" >}}
```

## Tabs

[Tabs](https://docs.gitlab.com/development/documentation/styleguide/#tabs)

```text
{{< tabs >}}

{{< tab title="Tab one" >}}

This is content in the first tab.

- This is a list
- It's inside a tab

{{< /tab >}}

{{< tab title="Tab two" >}}

We can render other shortcodes inside the tabs, like this alert box:

{{< /tab >}}

{{< alert type="note" >}}

Look at this box!

{{< /alert >}}

{{< /tabs >}}
```

# Redirects

The Docs website uses three different kinds of redirects to ensure that
outdated links continue to work for visitors. Maintaining old links is
important because many customers run older versions of the GitLab product,
which has older links to the Docs site from the UI.

The move to Hugo complicates redirect handling due to changes in how
Hugo renders paths using ["pretty" URLs](https://gohugo.io/content-management/urls/#appearance).

The Hugo site's redirect stack runs in the following order:

1. Cloudflare redirects (CDN/edge)
2. GitLab Pages redirects (web server)
3. Meta tag redirects (browser)

## Cloudflare

Cloudflare redirects fire at the CDN level, before a request reaches GitLab Pages. We use Cloudflare
to work around limitations with GitLab Pages redirects
(see [this issue](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/issues/96) for details).

Cloudflare handles two wildcard redirects:

1. Redirect URLs that end in `.html` to `/`.
   - Example: `docs.gitlab.com/ee/topics/plan_and_track.html` => `docs.gitlab.com/ee/topics/plan_and_track/`
1. Redirect URLs that have an `ee` prefix to the same path, without the `ee` prefix.
   - Example: `docs.gitlab.com/ee/topics/plan_and_track/` => `docs.gitlab.com/topics/plan_and_track/`

Cloudflare redirect rules are managed with Terraform and deployed to Cloudflare by
the GitLab infrastructure team. To request changes to these rules, create an issue
in the [Production Engineering issue queue](https://gitlab.com/gitlab-com/gl-infra/production-engineering/-/issues).

## GitLab Pages

GitLab Pages redirects work at the web server level. These redirects are created by
a script that we run as part of monthly maintenance tasks. This is not implemented
in Hugo yet (see [this issue](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/issues/29)).

These redirect rules are written to a plain text file, located at `static/_redirects`.

On compile, Hugo publishes this file to `public/_redirects`, where it is read
by the GitLab Pages web server.

See also: [GitLab Pages redirects](https://docs.gitlab.com/user/project/pages/redirects/).

## Meta tags

Pages that contain `redirect_to` in their front matter are rendered with a meta tag that
redirects the page to the specified new location.

These redirects expire after 3 months and are then converted to GitLab Pages redirects
as part of the maintenance task described above.

These pages also include the redirect location in their regular text content in order to
ensure a visitor browsing the in-product docs ("/help") can also find the new page
location.

See also: [Redirect to a page that already exists](https://docs.gitlab.com/development/documentation/redirects/#redirect-to-a-page-that-already-exists).

## More information

See [Redirects in GitLab Documentation](https://docs.gitlab.com/development/documentation/redirects/)
for information about adding redirects from a writer or contributor perspective.

# Docs site analytics

The Technical Writing team tracks website usage with Google Analytics.

GitLab team members can browse Google Analytics data on [this dashboard](https://lookerstudio.google.com/reporting/d6af7a2b-2aaa-4f30-8742-811e62777c93/page/p_ihbvblyl2c).

This dashboard includes data from [Elasticsearch](search.md), such as
popular search queries and the search result click rate.

Google Analytics code is added to the page using Google Tag Manager, which is
managed by the [GitLab Marketing Strategy & Analytics
team](https://about.gitlab.com/handbook/marketing/strategy-performance/).

For help with analytics tooling, create an issue in the [Marketing Analytics
project](https://gitlab.com/gitlab-com/marketing/marketing-strategy-performance/-/issues/new).

## Other scripts

* Bizible: Used by the GitLab marketing ops team to track the customer journey across GitLab websites. Loaded from [analytics.html](../themes/gitlab-docs/layouts/partials/analytics.html).
* LinkedIn: Used by the GitLab brand marketing team. Loaded from [analytics.html](../themes/gitlab-docs/layouts/partials/analytics.html).
* Marketo: Used by the GitLab marketing ops team to track web visits. Loaded from [analytics.html](../themes/gitlab-docs/layouts/partials/analytics.html).
* OneTrust: Provides privacy-related cookie settings. Loaded from [head.html](../themes/gitlab-docs/layouts/partials/head.html).
* [GitLab product analytics](https://docs.gitlab.com/operations/product_analytics/):
Experimental.
Loaded from [analytics.html](../themes/gitlab-docs/layouts/partials/analytics.html).

## Implementation

Analytics scripts are only included on the production domain and are excluded from
archived or self-hosted versions. Any new scripts should be loaded from the
[analytics.html](../themes/gitlab-docs/layouts/partials/analytics.html) template in order to maintain the
same conditional loading rules.

To test analytics scripts locally, run Hugo with the `ANALYTICS_ENABLED` environment variable:

```shell
ANALYTICS_ENABLED=true make view
```

## Deploy the site without analytics

In the event of a problem with analytics scripts on the production site,
project admins can deploy the site without analytics scripts:

1. Navigate to the Variables section of the [CI/CD settings page](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/settings/ci_cd).
2. Add a variable named `ANALYTICS_ENABLED` and set its value to `false`.
3. Run a new pipeline.
4. When the site deploys, it should no longer have analytics
scripts. You can test this by viewing the HTML source and searching for
strings like `googletagmanager`, `bizible`, `GL_PRODUCT_ANALYTICS_JSON`, etc.

# Search

GitLab Docs supports two search backends, Elasticsearch and Pagefind. Environment variables are passed at
build time to set the search backend.

- The primary production site, docs.gitlab.com, runs [Elasticsearch](https://www.elastic.co/elasticsearch).
- Archives, and self-hosted sites run [Pagefind](https://pagefind.app/).

## Elasticsearch

The Technical Writing team introduced Elasticsearch in Q3 2024 (see [epic](https://gitlab.com/groups/gitlab-org/-/epics/14747)).

### Implementation details

#### Infrastructure

The Elasticsearch instance for GitLab Docs runs on Elastic Cloud, alongside deployments for
GitLab.com. This is managed by GitLab infrastructure.

The deployment for GitLab Docs is labeled `gitlab-docs-website`.

##### Indices

Within our deployment, we have two production indexes:

- `search-gitlab-docs-hugo`: Indexes Hugo site content via the web crawler.
- `search-gitlab-docs-nanoc`: Indexes Nanoc site content via the web crawler.

These both have the same configuration:

- Site maps: `sitemap.xml`.
- Crawl rules: Disallow crawls for the homepage, the search page, and the 404 page.
- Scheduling: Crawl hourly.

Configuration for the index is stored in an [exported JSON file](../config/search/elasticsearch/search-index.json)
in this project. If you add or change a field, or field mappings, regenerate the file using
the console in the Elastic admin interface.
The file is the output of this API request: `GET /search-gitlab-docs-hugo`, where
`search-gitlab-docs-hugo` is the name of the index.

##### Admin access

If you need to access the Elastic Cloud admin console, you will need to file an Access Request
([example](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/31957)).

In addition to the indexes, the admin console is also used for:

- API key management
- Configuring CORS settings for authentication from the website. See
[Enabling CORS](https://www.elastic.co/docs/current/search-ui/tutorials/elasticsearch#enabling-cors)
for more information.

##### Manage API keys

Guidelines for managing keys:

- IMPORTANT: Keys must be read-only, limited to specific indexes, and
limited to specific clusters.
  - Enable "Control security privileges" when creating a new key.
  - Copy permissions from the [exported configuration file](../config/search/elasticsearch/key-permissions.json).
  - Production key permissions are validated in the [`check-elastic-permissions` CI job](../.gitlab/ci/security.gitlab-ci.yml).
- Issue individual API keys for developers who need to run queries locally. Set a one
year expiration date for developer keys.
- Rotate the production key periodically.

As this API key is used in frontend code, it is a public key, but it is still
good practice to use separate keys and rotate them regularly.

##### Authentication

We use the following environment variables to connect to Elasticsearch:
`ELASTIC_KEY` and `ELASTIC_INDEX`.

These are passed to the production site and review apps via CI variables.
GitLab Docs maintainers can change the values of these by navigating to Settings > CI/CD  > Variables
in the [GitLab Docs Hugo](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com) project.

We use the following variables to connect to Elasticsearch Behavior Analytics:
`ELASTIC_ANALYTICS_ENDPOINT`, `ELASTIC_ANALYTICS_COLLECTION_NAME`. These variables
come from the [Analytics Integration dashboard](https://gitlab-docs-website.kb.us-central1.gcp.cloud.es.io:9243/app/enterprise_search/analytics/collections/search-gitlab-docs-hugo-collection/integrate)
and are set as constants in our frontend code.

#### Frontend

Search forms are built with GitLab UI form components.
The forms make [API requests](../themes/gitlab-docs/src/services/elastic_api.js) to the
[Elasticsearch API](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html).

Search results appear beneath the form as you type on the homepage and interior pages.
They can also choose "View all results" to run their query from the [advanced search page](https://docs.gitlab.com/search).

The advanced search page includes all results in a paginated list view.
These are rendered alongside custom filters that use data from the
[section metatag](../themes/gitlab-docs/layouts/partials/head.html) to filter pages by navigation section.

##### Vue components

- [Search forms on the homepage and interior content pages](../themes/gitlab-docs/src/components/search/elastic_search_form.vue)
- [Search form and results on the advanced search page](../themes/gitlab-docs/src/components/search/elastic_results.vue)
- [Search filters on the advanced search page](../themes/gitlab-docs/src/components/search/search_filters.vue)

### Analytics

- [Elasticsearch Behavioral Analytics](https://gitlab-docs-website.kb.us-central1.gcp.cloud.es.io:9243/app/dashboards#/view/0a4acf73-0c3d-4e7a-9857-6a72bf698a7a?_g=()).
Currently, this data is only accessible to users who have access to Elastic Cloud.

## Pagefind Search

[Pagefind](https://pagefind.app/) is available as an alternative search backend for archived and self-hosted GitLab Docs
installations.
Pagefind search can also be used in offline or air-gapped environments.
Pagefind search requires an additional build step to create a search index.

Versions of the site running on <https://archives.docs.gitlab.com> use Pagefind search by default.

## Development

### Local environment

You can build your local site to use a specific search backend
by setting the `SEARCH_BACKEND` environment variable at compile time.

- Use Elasticsearch: `SEARCH_BACKEND="elastic" make view`. Default if `SEARCH_BACKEND` is not set.
- Use Pagefind search: Run an archive build. See [Develop for Archived Versions Locally](versions.md#develop-for-archived-versions-locally)
for details.

#### Local build with Elasticsearch

Querying Elasticsearch from your local environment requires the following environment variables:
`ELASTIC_KEY` and `ELASTIC_INDEX`.

Note that work on search UI components should be doable without Elastic access.

1. Request an API key by creating an issue in the `gitlab-docs` project.
1. Copy the `ELASTIC_INDEX` value from the project [CI/CD settings](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/settings/ci_cd).
1. Add the two environment variables to `~/.zshrc` (or whatever your shell settings file is) like this:

    ```shell
    export ELASTIC_INDEX="search-gitlab-docs-nanoc"
    export ELASTIC_KEY="abc123"
    ```

1. Restart your terminal, then run  `make view` to build and preview locally.

Alternatively, you can pass variables into the build like this:

```shell
ELASTIC_KEY="abc123" SEARCH_BACKEND="elastic" make view
```

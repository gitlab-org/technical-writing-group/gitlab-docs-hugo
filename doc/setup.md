# Set up local development and preview

Set up your workstation to preview content changes on the GitLab Docs website.

If you intend to do development work on the site, see
[GitLab docs site development](development.md) for additional setup
guidelines.

## Prerequisites

Prerequisites:

- [Git](https://git-scm.com)
- Make. For example, [GNU Make](https://www.gnu.org/software/make/).

If these prerequisites aren't installed:

- macOS users can install them by using [Homebrew](https://brew.sh), which is also useful for installing other packages.
- Linux users can to install them by using their distribution's package manager.

## Managing system dependencies

This project supports `mise` for installing required language runtimes and other system dependencies.

For information on installing `mise`, see <https://mise.jdx.dev/getting-started.html>. After you
install, make sure you [activate `mise`](https://mise.jdx.dev/getting-started.html#activate-mise)
and then restart your shell session.

If you don't install `mise`, you must install required language runtimes and other system dependencies yourself.
See [`.tool-versions`](../.tool-versions) for the required dependencies.

If you use `mise`, these dependencies will be installed automatically when you follow
the steps below.

## Set up your workstation

To set up your workstation for GitLab Docs:

1. Verify that you have `mise` installed if you're not installing system dependencies manually.
1. Clone this project (`docs-gitlab-com`).
1. In the `docs-gitlab-com` folder, run:

   ```shell
   make setup
   ```

1. The website build pulls content from project directories that are
   checked out in the same location as `docs-gitlab-com`. You can use your
   existing cloned repositories, or you can clone them all at once by running this command
   from within the `docs-gitlab-com` folder:

   ```shell
   make clone-docs-projects
   ```

   In either case, your resulting file structure should look like this:

   ```markdown
   .
   ├── charts-gitlab/
   ├── gitlab/
   ├── docs-gitlab-com/
   ├── gitlab-runner/
   ├── gitlab-operator/
   └── omnibus-gitlab/
   ```

1. Compile the site, and view a local preview:

   ```shell
   make view
   ```

You can now browse the site at [`http://localhost:1313`](http://localhost:1313). Changes you make to markdown content or
website source files should be visible immediately.

### Troubleshooting

If you encounter problems with your local site, run `make setup` first to make sure
dependencies are all installed and up-to-date.

If you get an error about a missing tool or dependency when you run `make setup`, check that `mise`
is installed and that it is [activated](https://mise.jdx.dev/getting-started.html#activate-mise)
correctly in your shell's configuration. If you update your shell configuration file, such as
`~/.zshrc`, make sure you close the shell window and open a new one to apply the change.

For further assistance, GitLab team members can reach out in the `#docs-tooling` Slack channel.
Community contributors can [file an issue](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/issues)
in the GitLab Docs project.

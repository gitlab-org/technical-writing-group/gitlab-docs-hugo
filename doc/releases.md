# Monthly documentation releases

When a new GitLab version is released on the third Thursday of the month, we release version-specific published
documentation for the new version.

The tasks described in this document cover the preparation steps and the publication steps. The
preparation steps are completed on your local computer. The publication steps are completed in the
GitLab UI.

When you've completed the documentation release process:

- The [online published documentation](https://docs.gitlab.com) includes:
  - The three most recent minor releases of the current major version. For example 17.7, 17.6, and
    17.5.
  - The most recent minor releases of the last two major versions. For example 16.11, and 15.11.
- Documentation updates after the third Thursday of the month are for the next release.

Each documentation release:

- Has a dedicated branch, named in the format `XX.yy`.
- Has a Docker image that contains a build of that branch.

For example:

- For [GitLab 17.6](https://docs.gitlab.com/17.6/), the
  [stable branch](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/tree/17.6) and Docker image:
  [`registry.gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/archives:17.6`](https://gitlab.com/gitlab-org/technical-writing-group/docs-gitlab-com/container_registry/8197615).
- For [GitLab 17.5](https://docs.gitlab.com/13.8/index.html), the
  [stable branch](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/tree/17.5) and Docker image:
  [`registry.gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/archives:17.5`](https://gitlab.com/gitlab-org/technical-writing-group/docs-gitlab-com/container_registry/8197615).

## Recommended timeline

To minimize problems during the documentation release process, use the following timeline:

- Complete the preparation steps in the week before the release. **All** of the following steps
  must be completed successfully before proceeding with the publication steps:

  1. If an issue was not already created for you by the TW that handled the last release,
     [create an issue for the release](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/issues/new?issuable_template=Default)
     to track your progress and ensure completeness.
  1. Check that the stable branches have been created in all 4 projects. For details, see "Check for
     the stable branches that correspond with the release" in the docs release issue. When all
     stable branches are available, proceed with the next step.
  1. Create a stable branch and Docker image for the new version. For details, see "Create a stable
     branch and Docker image for the release" in the docs release issue.

- Complete the publication steps on the third Thursday of the month, after the release post is live:

  1. [Create a release merge request](#create-release-merge-request) for the new version, which
     updates the versions list (`versions.json`) for the current documentation
     and adds the release to the Docker configuration.

  1. [Merge the release merge request and run the necessary Docker image builds](#merge-the-release-merge-request-and-run-the-docker-image-builds).

### Optional. Test locally

Prerequisite:

- Install Docker. To verify, run `which docker`.

1. Build the image and run it. For example, for GitLab 16.6 documentation:

   ```shell
   docker build -t docs:16.6 -f 16.6.Dockerfile . --build-arg VER=16.6
   docker run -it --rm -p 4000:4000 docs:16.6
   ```

   If you get a permission error, try running the commands prefixed with `sudo`.

   If you're informed that the Docker daemon isn't running, start it manually:

      - (MacOS) `dockerd` ([read more](https://docs.docker.com/config/daemon/#start-the-daemon-manually)).
      - (Linux) `sudo systemctl start docker` ([read more](https://docs.docker.com/config/daemon/systemd/#start-manually)).

1. Visit `http://localhost:4000/16.6/` to see if everything works correctly.
1. Stop the Docker container:
   1. Identify the container's ID with `docker container ls`.
   1. Run `docker stop <container ID>`.

If you run into problems using Docker on MacOS, see [MacOS Docker considerations](./development.md#macos-docker-considerations).

## Create release merge request

**Note:** An [epic is open](https://gitlab.com/groups/gitlab-org/-/epics/4361) to automate this step.

To create the release merge request for the release:

1. [ ] If `GITLAB_TOKEN` is not already set in your local environment, do the following steps first:
    1. [Create a personal access token](https://gitlab.com/-/user_settings/personal_access_tokens) with the `api` scope.
    1. Set the value of this token as an environment variable called `GITLAB_TOKEN` in your shell settings file.
      - For example, if you're using Zsh on MacOS, add this to `~/.zshrc`:
        `export GITLAB_TOKEN="your-token-value-here"`.
1. Make sure you're in the root path of the `docs-gitlab-com` repository.
1. Create merge request with updated versions by running the following command and verify:

      ```bash
        make create-release-merge-request VERSION={version in X.Y format}
      ```

    - Stashes local changes and updates `main` branch.
    - Creates a branch `release-X-Y`.
    - Updates lists of versions in [`content/versions.json`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/content/versions.json).
        - Example:
           - Sets `next` to the version number of the next release. For example, if you're releasing `15.2`, sets `next`
           to `15.3`.
           - Sets `current` to the version number of the release you're releasing. For example, if you're releasing `15.2`,
           sets `current` to `15.2`.
           - Ensures `last_major` is set to the two most recent major versions. Does not include the current major version.
             For example, if you're releasing `15.2`, ensures `last_major` is `14.10` and `13.12`.
           - Sets `last_minor` to the last two most recent minor releases. For example, if you're
             releasing `15.2`, sets `last_minor` to `15.1` and `15.0`.

           As a complete example, the `content/versions.json` file for the `15.2` release is:

           ```json
           [
             {
               "next": "15.3",
               "current": "15.2",
               "last_minor": ["15.1", "15.0"],
               "last_major": ["14.10", "13.12"]
             }
           ]
           ```

    - In [`.gitlab/ci/docker-images.gitlab-ci.yml`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/.gitlab/ci/docker-images.gitlab-ci.yml),
     under the test:image:docs-single job, it changes the `GITLAB_VERSION` variable to the version number of the release
     you're releasing.
    - Commits and pushes to create the merge request (but without running any `lefthook` tests), with a `~release` label.
    - Verify that the **Changes** tab includes the following:
      - `.gitlab/ci/docker-images.gitlab-ci.yml`
      - `content/versions.json`

## Merge the release merge request and run the Docker image builds

_Do this after the release post is live._

1. Verify that the [pipeline](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/pipelines) for
the stable branch (filter by branch)
   has passed and created a [Docker image](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/container_registry/8197615).
1. Open the [docs release merge request](#create-release-merge-request), mark it ready (that is, not draft), and merge it.
1. Go to the [scheduled pipelines page](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/pipeline_schedules)
   and run the `Build docker images pipeline (Manual)` pipeline.
1. Once previous pipeline is completed, run the `Build docs.gitlab.com every hour` scheduled pipeline.
   You don't need to run any jobs manually for this second pipeline.

## Post-deployment checklist

After the documentation is released, verify the documentation site has been deployed as expected.
Open site `docs.gitlab.com` in a browser and confirm both the latest version and the correct `pre-`
version are listed in the documentation version dropdown.

For example, if you released the 14.1 documentation, the first dropdown entry should be
`14.2`, followed by `14.1`.

## Troubleshooting

### `compile_prod` job fails when creating the docs stable branch

When you create the stable branch in the `docs-gitlab-com` project, the `compile_prod` job might fail.

This happens if stable branches have not been
created for all the related projects. Some of the stable branches are
created close to the third Thursday of the month, so you might need to run the pipeline of the
stable branch one more time before the release.

The error is similar to the following:

```shell
Error running Git command 'git [fetch --depth 1 origin heads/17-9-stable]': exit status 128
```

**Solution**: run a [new pipeline](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/pipelines/new)
targeting the docs stable branch after all upstream stable branches have been created.

### `build:compile_archive` and/or `image:docs-single` job fails when creating the docs stable branch

- **Not all upstream stable branches are created yet**

   The error is similar to the [`compile_prod` job failure](#compile_prod-job-fails-when-creating-the-docs-stable-branch),
   and might looks like this:

   ```shell
    [Info] Fetching gitlab on branch 17-9-stable-ee at commit heads/17-9-stable-ee
    Error running Git command 'git [fetch --depth 1 origin heads/17-9-stable-ee]': exit status 128
   ```

   **Solution**: run a [new pipeline](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/pipelines/new)
targeting the docs stable branch after all upstream stable branches have been created.

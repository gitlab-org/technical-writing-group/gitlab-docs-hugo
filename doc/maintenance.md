# GitLab docs site maintenance

Some of the issues that the GitLab technical writing team handles to maintain
`https://docs.gitlab.com` include:

- The deployment process.
- Temporary event or survey banners.

## Deployment process

We use [GitLab Pages](https://docs.gitlab.com/user/project/pages/) to build and host this website.

The site is built and deployed automatically in GitLab CI/CD jobs.
See [`.gitlab-ci.yml`](../.gitlab-ci.yml)
for the current configuration. The project has [scheduled pipelines](https://docs.gitlab.com/ci/pipelines/schedules/)
that build and deploy the site every hour.

## Survey banner

In case there's a survey that needs to reach a big audience, the docs site has
the ability to host a banner for that purpose. When it is enabled, it's shown
at the top of every interior page of the docs site.

To publish a survey, edit [`banner.yaml`](../data/en/banner.yaml) and:

1. Set `show_survey_banner` to `true`.
1. Under `description`, add what information you want to appear in the banner.
   Markdown is supported.

To unpublish a survey, edit [`banner.yaml`](../data/en/banner.yaml) and
set `show_survey_banner` to `false`.

## Review app tokens

The `docs-gitlab-com` project uses two access tokens for review apps:

- `DOCS_PROJECT_API_TOKEN` is a [project access token](https://docs.gitlab.com/user/project/settings/project_access_tokens/).
  Other projects use this token to poll the review app deployment pipeline and determine if the pipeline has completed
  successfully.
- `DOCS_TRIGGER_TOKEN` is a [trigger token](https://docs.gitlab.com/ci/triggers/#create-a-pipeline-trigger-token).
  Other projects use this token to authenticate with the `docs-gitlab-com` project and trigger a review app deployment
  pipeline.

All projects with documentation review apps use two of these tokens when running the
[`trigger-build` script](https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/trigger-build.rb)
to deploy a review app. Review apps stop working if you change or delete these tokens, or if the tokens expire.

### Regenerate `DOCS_HUGO_PROJECT_API_TOKEN` in `docs-gitlab-com`

Regenerate the `DOCS_HUGO_PROJECT_API_TOKEN` project access token when it expires or because of a security issue.
You can immediately revoke the existing token because it is used only for review apps or project maintenance.

Prerequisites:

- You must have at least the Maintainer role in the `docs-gitlab-com` project.

To regenerate `DOCS_HUGO_PROJECT_API_TOKEN`:

1. In [`docs-gitlab-com`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com), go to **Settings > Access tokens**.
1. In **Active project access tokens**, find the entry for `DOCS_HUGO_PROJECT_API_TOKEN` and select **Revoke**
   (**{remove}**).
1. Select **Add new token**, and fill in the following values:
   - **Token name**: `DOCS_HUGO_PROJECT_API_TOKEN`.
   - **Expiration date**: Set to be 12 months into the future.
   - **Select a role**: `Developer`.
   - **Select scopes**: `api`.
1. Select **Create project access token**.
1. After the token is created, go to **Your new project access token** at the top
   and copy the token value. It should start with `glpat-`.

#### Update `DOCS_HUGO_PROJECT_API_TOKEN` project access token in other projects

After regenerating the `DOCS_HUGO_PROJECT_API_TOKEN` project access token in the `docs-gitlab-com` project, you must
update the token in the projects that use it.

Prerequisites:

- You must have at least the Maintainer role in the project.

To update the `DOCS_HUGO_PROJECT_API_TOKEN` project access token in projects that use it:

1. On the left sidebar, select **Search or go to** and find each of the following projects.

   - `gitlab`
   - `gitlab-runner`
   - `omnibus-gitlab`
   - `charts`
   - `gitlab-operator`

1. In each project, select **Settings > CI/CD**.
1. Expand **Variables**.
1. On the `DOCS_HUGO_PROJECT_API_TOKEN` CI/CD variable, select **Edit** (**{pencil}**) and update the **Value** field
   with the new project access token. Don't change any other setting.

### Regenerate `DOCS_HUGO_TRIGGER_TOKEN` in `docs-gitlab-com`

Regenerate the `DOCS_HUGO_TRIGGER_TOKEN` pipeline trigger token when it expires or because of a security issue.
You can immediately revoke the existing token because it is used only for review apps or project maintenance.

Prerequisites:

- You must have at least the Maintainer role in the `docs-gitlab-com` project.

To regenerate `DOCS_HUGO_TRIGGER_TOKEN`:

1. In [`docs-gitlab-com`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com), go to **Settings > CI/CD**
and expand
   **Pipeline trigger tokens**.
1. In **Active pipeline trigger tokens**, find the entry for `DOCS_HUGO_TRIGGER_TOKEN` and select **Revoke trigger**
   (**{remove}**).
1. Select **Add new token**.
1. Under **Description**, fill in `DOCS_HUGO_TRIGGER_TOKEN`.
1. Select **Create pipeline trigger token**.
1. After the token is created, copy the token value from the table.

#### Update project trigger token in other projects

After regenerating the `DOCS_HUGO_TRIGGER_TOKEN` project trigger token in the `docs-gitlab-com` project, you must
update the token in the projects that use it.

Prerequisites:

- You must have at least the Maintainer role in the project.

To update the `DOCS_HUGO_TRIGGER_TOKEN` project trigger token in projects that use it:

1. On the left sidebar, select **Search or go to** and find each of the following projects.

   - `gitlab`
   - `gitlab-runner`
   - `omnibus-gitlab`
   - `charts`
   - `gitlab-operator`

1. In each project, select **Settings > CI/CD**.
1. Expand **Variables**.
1. On the `DOCS_HUGO_TRIGGER_TOKEN` CI/CD variable, select **Edit** (**{pencil}**) and update the **Value** field with
   the new project access token. Don't change any other setting.

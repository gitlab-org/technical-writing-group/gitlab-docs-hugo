# Version-aware features

A new release of the Docs website is cut with each release of the GitLab product.
Most of the site behaves the same way regardless of version,
however there are a few features that are version-aware:

- Archived version banner
- Versions dropdown
- Survey banner
- Search
- robots.txt

See the table below for expected behavior:

<!-- markdownlint-disable MD013 -->
| Version | Versions dropdown menu | Archived version banner | Survey banner | Search | robots.txt |
| -------- | ----- | ------- | ------- | ------ | ------ |
| Pre-release | Shows the same options as docs.gitlab.com. | No banner | Optional banner (interior pages only) | Google | Allow crawlers |
| Stable | Shows the same options as docs.gitlab.com. | No banner | No banner | Pagefind | Block crawlers |
| Recent (last two minors before stable) | Shows the same options as docs.gitlab.com. | Banner (interior pages only) | No banner | Pagefind | Block crawlers |
| Archive | Shows only the active version and a link back to docs.gitlab.com/archives  | Banner (interior pages only) | No banner | Pagefind | Block crawlers |
| Offline archive | Shows only the active version and a link back to docs.gitlab.com/archives | No banner | No banner | Pagefind | Block crawlers |
<!-- markdownlint-enable MD013 -->

## Environments for older releases

The pre-release version of the site is built from the `main` branch of this project and is available online at `docs.gitlab.com`.

Released versions need to run in three different places:

- `docs.gitlab.com/$VERSION` (e.g, docs.gitlab.com/16.7)
- `archives.docs.gitlab.com/$VERSION`
- a [self-hosted](https://docs.gitlab.com/administration/docs_self_host/) environment,
which may be running offline, but must have a URL in the format of `example.com/$VERSION`

When working on version-aware features, you will need to consider these different environments
and possibly test your work in multiple ways.

### Develop for archived versions locally

You can configure your local site to behave like an archive site,
or to actually build with older content, or both.

### Run a local site with an archive-style path

Run `make view-archive` with an environment variable for `CI_COMMIT_REF_NAME`, like this:

```shell
CI_COMMIT_REF_NAME=16.11 make view-archive
```

### Run a local site with older content

If you need to work on the site with older content,
check out the released branch in the relevant project
before running a build.

For example, to run the site with `16.11` GitLab content:

```shell
git -C ../gitlab fetch origin 16-11-stable-ee
git -C ../gitlab checkout 16-11-stable-ee
CI_COMMIT_REF_NAME=16.11 make view-archive
```

## Versioning logic

- **Backend:** The version of the site is set at build time from the `CI_COMMIT_REF_NAME`
environment variable and is set in a metatag on the default template.
On CI, this is the branch name (see [Predefined CI/CD variables reference](https://docs.gitlab.com/ci/variables/predefined_variables/)).

Remember that detecting the version from the backend does not allow you to determine the version
relative to current, or anything else that changes over time.
These values are captured at build time and not updated beyond that.

- **Frontend:** Frontend features can fetch up-to-date version context from the versions.json file at `docs.gitlab.com/versions.json`.
While this can be used to determine an instance's status (pre-release, stable, etc.),
remember that this (or any other `fetch` request) cannot be used in an offline environment.

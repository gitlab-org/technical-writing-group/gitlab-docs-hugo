const DOCS_VERSIONS_ENDPOINT = "https://docs.gitlab.com/versions.json";
const ARCHIVE_VERSIONS_ENDPOINT =
  "https://archives.docs.gitlab.com/archive_versions.json";

/**
 * Gets site version from meta tag.
 *
 * @returns String
 */
export const getSiteVersion = () => {
  return document
    .querySelector('meta[name="gitlab_docs_version"]')
    ?.getAttribute("content");
};

/**
 * Fetch a list of versions available on docs.gitlab.com.
 *
 * @returns Array
 */
let cachedVersions = null;

export async function getVersions() {
  if (!cachedVersions) {
    try {
      const data = await (await fetch(DOCS_VERSIONS_ENDPOINT)).json();
      cachedVersions = Object.assign(...data);
    } catch (error) {
      console.error(error);
    }
  }
  return cachedVersions || [];
}

/**
 * Fetch a list of site versions available on the Archives site.
 *
 * @returns Array
 */
export async function getArchivesVersions() {
  const versions = await fetch(ARCHIVE_VERSIONS_ENDPOINT)
    .then((response) => response.json())
    .catch((error) => console.error(error));
  return versions || [];
}

/**
 * Check if a version of the site is archived.
 *
 * All versions except the pre-release and most
 * recent stable version are considered archived
 * versions.
 *
 * Note that this will return false in an offline
 * or air-gapped environment.
 *
 * @returns Boolean
 */
export async function isOnlineArchivedVersion(version) {
  const onlineVersions = await getVersions();
  if (Object.keys(onlineVersions).length > 0) {
    return ![onlineVersions.next, onlineVersions.current].includes(version);
  }
  return false;
}

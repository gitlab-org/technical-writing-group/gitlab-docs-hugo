/* global ELASTIC_KEY */
/* global ELASTIC_INDEX */

import {
  getTracker,
  trackSearch,
} from "@elastic/behavioral-analytics-javascript-tracker";
import { SearchDriver } from "@elastic/search-ui";
import AnalyticsPlugin from "@elastic/search-ui-analytics-plugin";
import ElasticsearchAPIConnector from "@elastic/search-ui-elasticsearch-connector";
import { ELASTIC_CLOUD_ID } from "../search/constants";
import {
  cleanTitle,
  buildSearchQuery,
  buildSearchQueryConfig,
} from "../search/utils";
import { docsBaseURL } from "../utils/environment";

/**
 * Rewrite the path for result links
 *
 * This allows result links to work on
 * review apps or other environments where
 * the site is served in a subdirectory.
 */
const rewriteUrlPath = (rawUrl) => {
  const baseUrl = docsBaseURL();
  return baseUrl !== "/" ? `${baseUrl}${rawUrl.replace(/^\//, "")}` : rawUrl;
};

// Initialize connector to Elasticsearch instance.
const connector = new ElasticsearchAPIConnector(
  {
    cloud: {
      id: ELASTIC_CLOUD_ID,
    },
    index: ELASTIC_INDEX,
    apiKey: ELASTIC_KEY,
  },
  (requestBody, requestState, queryConfig) => {
    const updatedRequestBody = buildSearchQuery(
      requestBody,
      requestState,
      queryConfig,
    );
    return updatedRequestBody;
  },
);

/**
 * Fetch search results from Elastic
 *
 * @param {String} query
 * @param {Array} filters
 * @param {Object} pageInfo
 *   Contains pageNumber(int) and numResults(int)
 *
 * @returns Array
 */

export const fetchResults = (query, filters, pageInfo) => {
  return new Promise((resolve) => {
    if (!query || typeof query !== "string") {
      resolve([]);
      return;
    }

    // Flatten filters - needed for Elasticsearch required filter format.
    const flattenedFilters = filters.flatMap((filter) => filter.split(","));
    // Build filters and search query base config.
    const searchQuery = buildSearchQueryConfig(flattenedFilters);
    // Grab page information.
    const { pageNumber, numResults } = pageInfo || {};

    let tracker = null;

    try {
      tracker = getTracker();
      // eslint-disable-next-line no-unused-vars
    } catch (err) {
      console.info("Elasticsearch Analytics: Tracker not initialized.");
    }

    const driver = new SearchDriver({
      apiConnector: connector,
      searchQuery,
      initialState: {
        searchTerm: query,
        resultsPerPage: numResults,
        current: pageNumber,
      },
      trackUrlState: false,
      plugins:
        tracker != null
          ? [
              AnalyticsPlugin({
                client: tracker,
              }),
            ]
          : [],
    });

    let isInitialUpdate = true;

    // Set up a listener for results.
    driver.subscribeToStateChanges((state) => {
      if (isInitialUpdate) {
        isInitialUpdate = false;
        return;
      }
      if (state.isLoading === false && state.wasSearched === true) {
        const results = state.results.map((result) => ({
          id: result.id.raw,
          title: result.title.snippet
            ? cleanTitle(result.title.snippet[0])
            : cleanTitle(result.title.raw),
          breadcrumbs: result.gitlab_docs_breadcrumbs.raw,
          url_path: rewriteUrlPath(result.url_path.raw),
          htmlSnippet: result.body_content?.snippet[0],
        }));
        const resultsData = {
          results,
          pagingStart: state.pagingStart,
          pagingEnd: state.pagingEnd,
          totalResults: state.totalResults,
        };

        if (tracker != null) {
          trackSearch({
            search: {
              query,
              filters: filters.length
                ? {
                    gitlab_docs_section: flattenedFilters,
                  }
                : undefined,
              results: {
                total_results: state.totalResults,
              },
              page: {
                current: pageNumber,
                size: numResults,
              },
            },
          });
        }

        resolve(resultsData);
        driver.unsubscribeToStateChanges();
      }
    });

    // Trigger the search.
    driver.setSearchTerm(query);
    // Set current page to update results.
    driver.setCurrent(pageNumber);
  });
};

/**
 * Utilities for determining site environment.
 */

export const docsBaseURL = () =>
  document
    .querySelector('meta[name="gitlab_docs_base_url"]')
    ?.getAttribute("content");

/**
 * Check if we're on a production domain
 *
 * @returns Boolean
 */
export function isProduction() {
  const prodHosts = ["docs.gitlab.com", "localhost"];
  return prodHosts.includes(window.location.hostname);
}

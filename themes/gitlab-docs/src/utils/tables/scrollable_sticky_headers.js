import { StickyTableHeader } from "vh-sticky-table-header";

const SCROLL_RESTORATION_DELAY = 100; // milliseconds
const SCROLL_HELP_TEXT = "Scroll table to see more &rarr;";

/**
 * Initializes sticky headers for tables.
 *
 * We use the vh-sticky-table-header library here to allow
 * large tables to be horizontally scrollable, while maintaining
 * sticky headers. This is currently not possible with CSS.
 */
export const stickyScrollableTables = () => {
  const tables = document.querySelectorAll(".docs-content table");
  const mainContent = document.querySelector(".main-content");
  if (!tables.length || !mainContent) return;

  const isTableScrollable = (table) =>
    table.offsetWidth > mainContent.offsetWidth;
  const createScrollHint = () =>
    `<div class="gl-italic gl-text-sm gl-text-right">${SCROLL_HELP_TEXT}</div>`;

  // Add extra markup required for vh-sticky-table-header
  // See https://github.com/archfz/vh-sticky-table-header/blob/main/README.md
  tables.forEach((table) => {
    const tableClasses = table.className;
    const initialWidth = `${table.offsetWidth}px`;

    table.style.width = initialWidth;

    table.outerHTML = `
      ${isTableScrollable(table) ? createScrollHint() : ""}
      <div class="sticky-table-main table-container">${table.outerHTML}</div>
      <div class="sticky-table-clone table-container"><table class="${tableClasses}"></table></div>`;
  });

  // Handling for window resize
  window.addEventListener("resize", () => {
    requestAnimationFrame(() => {
      document.querySelectorAll(".sticky-table-main table").forEach((table) => {
        /**
         * Preserve table widths during window resize
         *
         * When sorting tables, we replace the tbody element with a new one,
         * which can cause the table to lose its width constraints.
         * This is particularly problematic when combined with the sticky header library,
         * which needs stable table dimensions to maintain proper alignment between the
         * original table and its clone.
         *
         * By explicitly storing and restoring the table width during resize,
         * we prevent unwanted table reflow and keep the sticky headers properly aligned.
         */
        const currentWidth = `${table.style.width || table.offsetWidth}px`;

        // Update scroll hints on resize
        const hint = table.closest(".sticky-table-main").previousElementSibling;
        if (
          isTableScrollable(table) &&
          !hint?.classList.contains("gl-italic")
        ) {
          table
            .closest(".sticky-table-main")
            .insertAdjacentHTML("beforebegin", createScrollHint());
        } else if (
          !isTableScrollable(table) &&
          hint?.classList.contains("gl-italic")
        ) {
          hint.remove();
        }

        // Restore width
        table.style.width = currentWidth;
      });
    });
  });

  const mainTables = document.querySelectorAll(".sticky-table-main table");
  const cloneTables = document.querySelectorAll(".sticky-table-clone table");

  mainTables.forEach((mainTable, index) => {
    const cloneTable = cloneTables[index];
    if (!mainTable || !cloneTable) return;

    // Sticky header library errors if <thead> is missing. Skip the non-standard tables here.
    const noHeaderTable = !mainTable.querySelector("thead");
    if (noHeaderTable) {
      return;
    }

    // Initialize sticky header with header height offset
    const headerHeight = 48; // matches CSS variable --header-height: 3rem (48px)
    const sticky = new StickyTableHeader(mainTable, cloneTable, {
      max: headerHeight,
    });

    /*
     * Override the library's position calculation.
     * The method the library uses this does not work with our grid layout,
     * fixed header, and scrollable sidebars.
     */
    // eslint-disable-next-line func-names
    sticky.setupSticky = function () {
      const updateSticky = () => {
        this.currentFrameRequest = window.requestAnimationFrame(() => {
          const headerRect = this.header.getBoundingClientRect();

          // Show clone when original header is between the top offset and bottom of table
          const shouldShow =
            headerRect.top < headerHeight &&
            headerRect.top > -mainTable.offsetHeight;

          if (shouldShow) {
            this.cloneContainerParent.style.display = "block";
            this.cloneContainerParent.style.position = "fixed";
            this.cloneContainerParent.style.top = `${headerHeight}px`;
            this.setHorizontalScrollOnClone();
          } else {
            this.cloneContainerParent.style.display = "none";
          }
        });
      };

      // Set up scroll listener
      this.scrollListener = () => updateSticky();

      // Hide clone initially
      this.cloneContainerParent.style.display = "none";

      // Wait a brief moment for scroll restoration to complete
      setTimeout(() => {
        updateSticky(); // Initialize after potential scroll restoration
        window.addEventListener("scroll", this.scrollListener);
      }, SCROLL_RESTORATION_DELAY);
    };

    // Initialize with our custom positioning
    sticky.setupSticky();
  });
};

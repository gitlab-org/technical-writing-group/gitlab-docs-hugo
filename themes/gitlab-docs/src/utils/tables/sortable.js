import { compareVersions } from "compare-versions";

// Constants.
const NOT_DEFINED = "Not defined";
const SORT_ORDERS = {
  DEFAULT: "default",
  ASC: "asc",
  DESC: "desc",
};

const updateTableBody = (tbody, rows) => {
  const newTbody = document.createElement("tbody");
  rows.forEach((row) => newTbody.appendChild(row));
  tbody.parentNode.replaceChild(newTbody, tbody);
};

const compareData = (dataInfo, order) => {
  const { dataA, dataB, dataType } = dataInfo || {};

  // Sort numbers with compare version.
  if (dataType === "number") {
    if (dataA === NOT_DEFINED) return dataB === NOT_DEFINED ? 0 : 1;
    if (dataB === NOT_DEFINED) return -1;

    return order === SORT_ORDERS.DESC
      ? compareVersions(dataB, dataA)
      : compareVersions(dataA, dataB);
  }

  // Sort strings.
  const comparison = dataA > dataB ? 1 : -1;
  return order === SORT_ORDERS.DESC ? -comparison : comparison;
};

// sortTable sorts rows based on type of data.
const sortTable = (table, { column, order, dataType } = {}) => {
  const tbody = table.querySelector("tbody");
  if (!tbody) return; // Exit early if no tbody

  const rows = [...tbody.querySelectorAll("tr")].sort((a, b) => {
    const [dataA, dataB] = [a, b].map((row) =>
      row.children[column].textContent.trim(),
    );
    return compareData({ dataA, dataB, dataType }, order);
  });

  updateTableBody(tbody, rows);
};

// addSortDirection adds/removes appropriate sorting classes to show the direction of sorting.
export const addSortDirection = (table) => {
  const headers = table.querySelectorAll("th.sortable");
  headers.forEach((header) => {
    // If data-default is provided, add sorted-desc or sorted-asc class
    // depending on what is provided, otherwise sorted-default.
    header.classList.add(
      `sorted-${header.dataset.default || "default"}`.replace("ending", ""),
    );

    header.addEventListener("click", () => {
      // If order was already asc -> make it desc.
      // If order was already desc -> make it asc.
      // By default -> asc.
      let order;
      if (header.classList.contains("sorted-asc")) {
        order = SORT_ORDERS.DESC;
      } else if (header.classList.contains("sorted-desc")) {
        order = SORT_ORDERS.ASC;
      } else {
        order = SORT_ORDERS.ASC;
      }

      // Update classes for all headers.
      headers.forEach((h) => {
        const eachHeader = h;
        // Remove the data-default attribute if it exists after use click.
        if (
          eachHeader.dataset.default === "desc" ||
          eachHeader.dataset.default === "asc"
        ) {
          delete eachHeader.dataset.default;
        }

        // If current is the clicked header -> add the appropriate sort order and reset other headers to default.
        eachHeader.className =
          h === header ? `sortable sorted-${order}` : "sortable sorted-default";
      });

      const columnIndex = Array.from(header.parentNode.children).indexOf(
        header,
      );

      // Allow sortable headers to work alongside scrollable_sticky_headers.js.
      const cloneTable =
        headers[0].closest(".sticky-table-main")?.nextElementSibling;
      if (cloneTable?.matches(".sticky-table-clone")) {
        const cloneHeaders = cloneTable.querySelectorAll("th.sortable");
        cloneHeaders?.forEach((cloneHeader, index) => {
          cloneHeader.className = headers[index].className;
        });
      }

      // Sort the table.
      sortTable(table, {
        column: columnIndex,
        order,
        dataType: header?.dataset?.sort,
      });
    });
  });

  // Initial sort for the default descending or ascending column.
  const defaultDescHeader = Array.from(headers).find(
    (h) => h.dataset.default === "desc",
  );
  const defaultAscHeader = Array.from(headers).find(
    (h) => h.dataset.default === "asc",
  );
  if (defaultDescHeader) {
    const columnIndex = Array.from(
      defaultDescHeader.parentNode.children,
    ).indexOf(defaultDescHeader);
    sortTable(table, {
      column: columnIndex,
      order: SORT_ORDERS.DESC,
      dataType: defaultDescHeader?.dataset?.sort,
    });
  } else if (defaultAscHeader) {
    const columnIndex = Array.from(
      defaultAscHeader.parentNode.children,
    ).indexOf(defaultAscHeader);
    sortTable(table, {
      column: columnIndex,
      order: SORT_ORDERS.ASC,
      dataType: defaultAscHeader?.dataset?.sort,
    });
  }
};

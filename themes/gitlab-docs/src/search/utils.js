import {
  getTracker,
  trackSearchClick,
} from "@elastic/behavioral-analytics-javascript-tracker";

/**
 * Search filters.
 *
 * Option properties:
 *   - text: Used for checkbox labels
 *   - value: References values in the "docs-site-section" metatag, which is included each search result.
 *   - id: Machine-friendly version of the text, used for analytics and URL params.
 */
export const SEARCH_FILTERS = [
  {
    title: "Filter by",
    options: [
      {
        text: "Tutorials",
        value: "tutorials",
        id: "tutorials",
      },
      {
        text: "Installation docs",
        value: "install,subscribe",
        id: "install",
      },
      {
        text: "Administration docs",
        value: "administer,subscribe",
        id: "administer",
      },
      {
        text: "User docs",
        value: "subscribe,use_gitlab,gitlab_duo",
        id: "user",
      },
      {
        text: "Extension and API docs",
        value: "extend",
        id: "extend",
      },
      {
        text: "Contributor docs",
        value: "contribute",
        id: "contribute",
      },
      {
        text: "Solution docs",
        value: "solutions",
        id: "solutions",
      },
    ],
  },
];

/**
 * Boost ranking for specific pages for specific terms.
 */
const BOOSTED_PAGES = [
  {
    path: "/user/gitlab_duo_chat/examples/",
    queries: ["duo slash commands"],
  },
];

/**
 * Check URL parameters for search parameters.
 *
 * We support "q" for the query string as it's a search standard,
 * and also "query" as it has been long-documented in the
 * GitLab handbook as a Docs search parameter.
 *
 * See https://about.gitlab.com/handbook/tools-and-tips/searching/
 *
 * @returns
 *  An object containing query parameters.
 */
export const getSearchParamsFromURL = () => {
  const searchParams = new URLSearchParams(window.location.search);
  return {
    qParam: searchParams.get("q") || searchParams.get("query") || "",
    pageParam: searchParams.get("page") || "",
    filterParam: searchParams.get("filters") || "",
  };
};

/**
 * Update URL parameters.
 *
 * This allows users to retrace their steps after a search.
 *
 * @param params Object
 *   Key/value pairs with the param name and value.
 *   Values can be strings or arrays.
 */
export const updateURLParams = (params) => {
  const queryString = Object.entries(params)
    .filter(
      ([, value]) =>
        value !== "" && !(Array.isArray(value) && value.length === 0),
    )
    .map(
      ([key, value]) =>
        `${encodeURIComponent(key)}=${encodeURIComponent(value)}`,
    )
    .join("&");
  window.history.pushState(
    null,
    "",
    `${window.location.pathname}?${queryString}`,
  );
};

/**
 * Convert between filter values and filter IDs.
 *
 * @param Array arr
 *   Selected filters to convert.
 * @param Boolean isToID
 *   true to convert to IDs, false to convert to values
 *
 * @returns Array
 */
export const convertFilterValues = (arr, isToID) => {
  const convertedArr = arr.map((item) => {
    for (const filter of SEARCH_FILTERS) {
      for (const option of filter.options) {
        if (
          (isToID && option.value === item) ||
          (!isToID && option.id === item)
        ) {
          return isToID ? option.id : option.value;
        }
      }
    }
    return null;
  });
  return convertedArr.filter((item) => item !== null);
};

/**
 * Tracks search result clicks and updates Elasticsearch analytics.
 *
 * @param {Event} event - The click event
 * @param {Object} result - The search result that was clicked
 * @param {string} query - The search query
 * @param {number} pageNumber - The current page number
 *
 * @returns {boolean} - Return true to allow the default link behavior
 */
export const trackSearchResultClick = (event, result, query, pageNumber) => {
  const isModifierClick = event.ctrlKey || event.metaKey || event.button === 1;

  try {
    // Always track the click for analytics
    const tracker = getTracker();
    if (tracker) {
      trackSearchClick({
        page: {
          url: result.url_path,
          title: result.title,
        },
        search: {
          query,
          page: { current: pageNumber, size: 10 },
        },
      });
    }
  } catch (err) {
    console.warn(err);
  }

  // For normal clicks only, prevent default and handle navigation
  if (!isModifierClick) {
    event.preventDefault();

    // Navigate after tracking completes
    setTimeout(() => {
      window.location.href = result.url_path;
    }, 250);
  }
  // For modifier clicks, do nothing, and let browser handle it
};

/**
 * Keyboard shortcuts.
 */
export const activateKeyboardShortcut = () => {
  document.addEventListener("keydown", (e) => {
    // Focus on the search form with the forward slash and S keys.
    const shortcutKeys = ["/", "s"];
    if (!shortcutKeys.includes(e.key) || e.ctrlKey || e.metaKey) return;
    if (/^(?:input|textarea|select|button)$/i.test(e.target.tagName)) return;
    e.preventDefault();
    document.querySelector('input[type="search"]').focus();
  });
};

/**
 * Remove formatting and boilerplate text from a page title
 *
 * We use this when we want to reference the title of a page,
 * without any extra formatting.
 */
export const cleanTitle = (htmlTitle) => {
  return (
    htmlTitle
      // Remove any text that starts with " | "
      .replace(/\s*\|.*$/, "")
      // Some pages use backticks to style words in titles as code.
      // We don't want to include these in places where we aren't parsing markdown
      .replaceAll("`", "")
      .trim()
  );
};

/**
 * Creates base query config for Elasticsearch.
 */
export const buildSearchQueryConfig = (filters) => {
  const searchQuery = {
    result_fields: {
      title: {
        raw: {},
        snippet: {
          size: 100,
          fallback: true,
        },
      },
      url_path: { raw: {} },
      gitlab_docs_breadcrumbs: { raw: {} },
      gitlab_docs_section: { raw: {} },
    },
    search_fields: {
      title: { weight: 3 },
      headings: { weight: 2 },
      body_content: { weight: 1 },
    },
    filters: [],
  };

  // Override search query filters.
  if (filters.length > 0) {
    searchQuery.filters = [
      {
        field: "gitlab_docs_section",
        values: filters,
        type: "any",
      },
    ];
  }

  return searchQuery;
};

/**
 * Creates final request body for Elasticsearch.
 */
export const buildSearchQuery = (requestBody, requestState, queryConfig) => {
  if (!requestState.searchTerm || typeof requestState.searchTerm !== "string") {
    return null;
  }

  const searchTerm = requestState.searchTerm.toLowerCase();

  // Check if current search term matches any of our boosted pages
  const boostMatch = BOOSTED_PAGES.find((page) =>
    page.queries.some((query) => query.toLowerCase() === searchTerm),
  );

  const searchFields = queryConfig.search_fields;

  /**
   * Match clauses for each important field (title, body_content, headings).
   * Each match query request, the term to match and if it does, it'll be boosted based on the boost value provided.
   * "title" is given the highest boost as per search results standards.
   */
  const boostMap = {
    title: 4,
    body_content: 3,
    headings: 2,
  };
  const matchObjectsWithBoosts = Object.entries(boostMap).map(
    ([field, boost]) => ({
      match: {
        [field]: {
          query: requestState.searchTerm,
          operator: "and",
          boost,
        },
      },
    }),
  );

  const updatedRequestBody = {
    ...requestBody,
    query: {
      function_score: {
        query: {
          bool: {
            // Must clause ensures all conditions within must be satisfied.
            must: [
              {
                bool: {
                  // Should clause makes sure that any matches for these conditions will be returned.
                  // If all or most conditions are matched, those results will be ranked higher.
                  should: [
                    {
                      multi_match: {
                        query: requestState.searchTerm,
                        fields: Object.keys(searchFields).map((fieldName) => {
                          const weight = searchFields[fieldName].weight || 1;
                          return `${fieldName}^${weight}`;
                        }),
                        type: "best_fields",
                        tie_breaker: 0.3,
                        boost: 1,
                      },
                    },
                    ...matchObjectsWithBoosts,
                  ],
                },
              },
            ],
          },
        },
        functions: [
          {
            // De-boost dev docs, and pages not in the nav.
            filter: {
              terms: { gitlab_docs_section: ["contribute", "none"] },
            },
            weight: 0.5,
          },
        ],
        score_mode: "multiply",
      },
    },
    highlight: {
      type: "plain",
      encoder: "html",
      boundary_scanner: "sentence",
      fragment_size: 200,
      no_match_size: 300,
      number_of_fragments: 1,
      max_analyzed_offset: 999999,
      pre_tags: "<strong>",
      post_tags: "</strong>",
      fields: {
        body_content: {},
        title: {},
      },
    },
  };

  // Add filters.
  if (queryConfig?.filters?.length) {
    updatedRequestBody.query.function_score.query.bool.must.push({
      bool: {
        should: [],
      },
    });

    const [filter] = queryConfig?.filters || [];
    const { field, values } = filter || {};

    if (field && values) {
      const mustArrLen =
        updatedRequestBody.query.function_score.query.bool.must.length;
      updatedRequestBody.query.function_score.query.bool.must[
        mustArrLen - 1
      ].bool.should.push(...values.map((val) => ({ term: { [field]: val } })));
    }
  }

  // If we have a boost match, add a strong boost for the specific page
  if (boostMatch) {
    updatedRequestBody.query.function_score.functions.push({
      filter: {
        term: {
          url_path: boostMatch.path,
        },
      },
      weight: 1000,
    });
  }

  return updatedRequestBody;
};

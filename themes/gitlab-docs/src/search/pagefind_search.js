import Vue from "vue";
import PageFindSearchForm from "../components/search/pagefind_search_form.vue";
import { activateKeyboardShortcut } from "./utils";

document.addEventListener("DOMContentLoaded", () => {
  activateKeyboardShortcut();

  // Homepage and interior navbar search forms
  (() =>
    new Vue({
      el: ".js-pagefind-search-form",
      components: {
        PageFindSearchForm,
      },
      render(createElement) {
        return createElement(PageFindSearchForm);
      },
    }))();
});

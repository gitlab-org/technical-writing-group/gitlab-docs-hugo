import { setCookie, getCookie } from "../utils/cookies";
import { cleanTitle } from "./utils";

/**
 * Store recent page views in a cookie.
 */

// Number of links to include in history
export const RECENT_HISTORY_ITEMS = 4;

// Writes page URLs to a cookie
export const trackPageHistory = () => {
  let pageHistory = [];
  const currentPath = window.location.pathname;
  const cookieValue = getCookie("pageHistory");

  if (cookieValue) {
    try {
      pageHistory = JSON.parse(cookieValue);
      if (!Array.isArray(pageHistory)) {
        pageHistory = [];
      }
    } catch {
      pageHistory = [];
    }
  }

  // Remove current page URL if it already exists
  const index = pageHistory.findIndex((item) => item.path === currentPath);
  if (index > -1) {
    pageHistory.splice(index, 1);
  }

  // Add the current page URL to the beginning of the history array
  pageHistory.unshift({
    path: currentPath,
    title: cleanTitle(document.title),
  });

  // Keep only the designated amount of pages in history
  if (pageHistory.length > RECENT_HISTORY_ITEMS) {
    pageHistory = pageHistory.slice(0, RECENT_HISTORY_ITEMS);
  }

  // Set a cookie with the history string
  try {
    const updatedCookieValue = JSON.stringify(pageHistory);
    setCookie("pageHistory", updatedCookieValue, 365);
  } catch {
    // Skip updating if data can't be stringified
  }
};

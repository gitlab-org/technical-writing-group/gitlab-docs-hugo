import Vue from "vue";
import ElasticResults from "../components/search/elastic_results.vue";
import { activateKeyboardShortcut } from "./utils";

document.addEventListener("DOMContentLoaded", () => {
  activateKeyboardShortcut();

  (() =>
    new Vue({
      el: ".js-search-results",
      components: {
        ElasticResults,
      },
      render(createElement) {
        return createElement(ElasticResults);
      },
    }))();
});

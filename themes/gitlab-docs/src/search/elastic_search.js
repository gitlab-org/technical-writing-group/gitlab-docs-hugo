/* global ELASTIC_KEY */

import Vue from "vue";
import { createTracker } from "@elastic/behavioral-analytics-javascript-tracker";
import ElasticSearchForm from "../components/search/elastic_search_form.vue";
import {
  ELASTIC_ANALYTICS_ENDPOINT,
  ELASTIC_ANALYTICS_COLLECTION_NAME,
} from "./constants";
import { activateKeyboardShortcut } from "./utils";

document.addEventListener("DOMContentLoaded", () => {
  activateKeyboardShortcut();

  // Initialize tracker if necessary variables are available.
  if (
    ELASTIC_ANALYTICS_ENDPOINT !== "" &&
    ELASTIC_ANALYTICS_COLLECTION_NAME !== "" &&
    ELASTIC_KEY !== ""
  ) {
    createTracker({
      endpoint: ELASTIC_ANALYTICS_ENDPOINT,
      collectionName: ELASTIC_ANALYTICS_COLLECTION_NAME,
      apiKey: ELASTIC_KEY,
    });
  }

  (() =>
    new Vue({
      el: ".js-elastic-search-form",
      components: { ElasticSearchForm },
      render(createElement) {
        return createElement(ElasticSearchForm, {
          props: {
            numResults: 7,
          },
        });
      },
    }))();
});

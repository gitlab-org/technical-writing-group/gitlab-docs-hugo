import Vue from "vue";
import PageFindSearchResults from "../components/search/pagefind_search_results.vue";
import { activateKeyboardShortcut } from "./utils";

document.addEventListener("DOMContentLoaded", () => {
  activateKeyboardShortcut();

  // Search results page (/search)
  (() =>
    new Vue({
      el: ".js-pagefind-search-results",
      components: { PageFindSearchResults },
      render(createElement) {
        return createElement(PageFindSearchResults);
      },
    }))();
});

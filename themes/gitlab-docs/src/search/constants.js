// Elasticsearch constants.
export const ELASTIC_CLOUD_ID =
  "gitlab-docs-website:dXMtY2VudHJhbDEuZ2NwLmNsb3VkLmVzLmlvJDQwZTQyYTQzMTJiZjQyMzNiMzBiZTg0MTU5YjlkNmE1JGMxODg4Y2U5OTY0YzQzZjc5ZjQ1YTk5NDZmMjI0ODg0";

// Elasticsearch Behavioral Analytics constants.
export const ELASTIC_ANALYTICS_ENDPOINT =
  "https://40e42a4312bf4233b30be84159b9d6a5.us-central1.gcp.cloud.es.io:443";
export const ELASTIC_ANALYTICS_COLLECTION_NAME =
  "search-gitlab-docs-hugo-collection";

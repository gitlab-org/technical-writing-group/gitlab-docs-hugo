import tocbot from "tocbot";
import Vue from "vue";
import "../assets/css/main.css";
import ClipboardCopy from "./components/clipboard_copy.vue";
import DocsBadges from "./components/docs_badge.vue";
import DocsBanner from "./components/docs_banner.vue";
import SidebarMenu from "./components/sidebar_menu.vue";
import TabbedContent from "./components/tabbed_content.vue";
import VersionsMenu from "./components/versions_menu.vue";
import { expandCollapse } from "./features/collapse";
import { trackPageHistory } from "./search/history";
import { getSiteVersion, isOnlineArchivedVersion } from "./services/versions";
import { getNextUntil } from "./utils/dom";
import { docsBaseURL } from "./utils/environment";
import { stickyScrollableTables } from "./utils/tables/scrollable_sticky_headers";
import { addSortDirection } from "./utils/tables/sortable";

const GRAPHQL_API_PAGE_PATH = "/api/graphql/reference/";

/**
 * Adds a clickable permalink to each content heading
 */
const addHeaderPermalinks = () => {
  document
    .querySelectorAll(".docs-content h2, h3, h4, h5, h6")
    .forEach((heading) => {
      const { id } = heading;
      if (!id) return;

      const anchor = document.createElement("a");
      anchor.href = `#${id}`;
      anchor.title = "Permalink";
      anchor.classList.add("anchor");

      heading.appendChild(anchor);
    });
};

document.addEventListener("DOMContentLoaded", async () => {
  expandCollapse();
  addHeaderPermalinks();
  trackPageHistory();

  // Allow large tables to be both scrollable and have sticky headers.
  // This has performance issues on pages with thousands of tables,
  // like the GraphQL reference page, so we need to skip it there.
  if (!window.location.pathname.endsWith(GRAPHQL_API_PAGE_PATH)) {
    stickyScrollableTables();
  }

  /**
   * Set up the table of contents
   * @see https://tscanlin.github.io/tocbot/
   */
  tocbot.init({
    tocSelector: ".js-toc",
    positionFixedSelector: ".js-toc",
    contentSelector: ".docs-content",
    headingSelector: "h2, h3, h4",
    ignoreSelector: ".help-feedback h2, .help-feedback h3, h3.archive-item",
    collapseDepth: 6, // Uncollapse all headers.
    // These should match our header-offset CSS variable.
    // header-offset = 4rem = 64px
    headingsOffset: 64,
    scrollSmoothOffset: -64,
  });

  // Highlight the clicked ToC link, even if it's not at the top of the page.
  // Only headings that appear at the top of the page are highlighted automatically.
  document.querySelectorAll(".toc-link").forEach((header) => {
    header.addEventListener("click", () => {
      document
        .querySelector(".is-active-link")
        ?.classList.remove("is-active-link");
      header.classList.add("is-active-link");
    });
  });

  // Initialize Vue components
  // Survey banner
  const bannerContainer = document.querySelector(
    '[data-vue-app="survey-banner"]',
  );
  (() =>
    new Vue({
      el: bannerContainer,
      components: {
        DocsBanner,
      },
      render(createElement) {
        return createElement(DocsBanner, {
          props: {
            bannerType: "survey",
            text: bannerContainer.innerHTML,
            variant: "info",
          },
        });
      },
    }))();

  // Archive banner
  const siteVersion = getSiteVersion();
  if (await isOnlineArchivedVersion(siteVersion)) {
    document.body.classList.add("has-archive-banner");

    // Create the link to the latest page by dropping the version number from the URL path.
    const latestURL = `https://docs.gitlab.com/${window.location.pathname.replace(`/${siteVersion}/`, "")}`;
    (() =>
      new Vue({
        el: document.querySelector("#js-version-banner"),
        components: {
          DocsBanner,
        },
        render(createElement) {
          return createElement(DocsBanner, {
            props: {
              bannerType: "archive",
              text: `This is <a href="https://docs.gitlab.com/archives">archived documentation</a> for GitLab. Go to
          <a href="${latestURL}">the latest</a>.`,
              variant: "tip",
              dismissible: false,
            },
          });
        },
      }))();
  }

  // Sidebar menu
  const menuContainer = document.querySelector('[data-vue-app="sidebar-menu"]');
  (() =>
    new Vue({
      el: menuContainer,
      components: {
        SidebarMenu,
      },
      render(createElement) {
        return createElement(SidebarMenu, {
          props: {
            baseUrl: docsBaseURL(),
          },
        });
      },
    }))();

  // Versions menu
  const versionsContainer = document.querySelector(
    '[data-vue-app="versions-menu"]',
  );
  (() =>
    new Vue({
      el: versionsContainer,
      components: {
        VersionsMenu,
      },
      render(createElement) {
        return createElement(VersionsMenu);
      },
    }))();

  // Tabs
  const tabsetSelector = '[data-vue-app="docs-tabs"]';
  document.querySelectorAll(tabsetSelector).forEach((tabset) => {
    const tabTitles = [];
    const tabContents = [];

    tabset.querySelectorAll(".tab-title").forEach((tab) => {
      tabTitles.push(tab.innerText);
      tabContents.push(getNextUntil(tab, ".tab-title"));
    });

    const tabsContainer = document.querySelector(tabsetSelector);
    (() =>
      new Vue({
        el: tabsContainer,
        components: {
          TabbedContent,
        },
        render(createElement) {
          return createElement(TabbedContent, {
            props: {
              tabTitles,
              tabContents,
              responsive: false,
            },
          });
        },
      }))();
  });

  // Clipboard copy
  document.querySelectorAll(".codeblock-wrapper").forEach((codeblock) => {
    (() =>
      new Vue({
        el: codeblock.querySelector('[data-vue-app="codeblock-toolbar"]'),
        components: {
          ClipboardCopy,
        },
        render(createElement) {
          const toolbarElement = codeblock.querySelector(
            '[data-vue-app="codeblock-toolbar"]',
          );

          return createElement(ClipboardCopy, {
            props: {
              codeContent: codeblock.querySelector("pre").textContent,
              codeLanguage:
                toolbarElement.getAttribute("data-code-language") || "",
            },
          });
        },
      }))();
  });

  // Contribute/Solutions badge for h1 headers
  const badgeSpan = document.querySelector('[data-component="docs-badges"]');
  if (badgeSpan) {
    const badge = badgeSpan.querySelector("span");
    const badgeData = {
      text: badge.getAttribute("data-value"),
      hoverText: badge.getAttribute("data-hover-text"),
    };
    (() =>
      new Vue({
        el: badge,
        components: {
          DocsBadges,
        },
        render(createElement) {
          return createElement(DocsBadges, {
            props: { badgeData },
          });
        },
      }))();
  }

  // Add sorting to feature flags tables.
  const tables = document.querySelectorAll(".table-sortable");
  tables.forEach((table) => {
    addSortDirection(table);
  });
});

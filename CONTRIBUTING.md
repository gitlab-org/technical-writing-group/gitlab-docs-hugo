# Developer Certificate of Origin + License

By contributing to GitLab Inc., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab Inc.
Except for the license granted herein to GitLab Inc. and recipients of software
distributed by GitLab Inc., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

<!-- _This notice should stay as the first item in the CONTRIBUTING.md file._ -->

## Ownership

[Susan Tacker](https://gitlab.com/susantacker), Director, Technical Writing, is the DRI for this project.

## Contribution process

When creating issues, merge requests, and epics related to <https://docs.gitlab.com>:

1. Add the label ~"Category:Docs Site".
1. Add the labels ~frontend, ~backend, or ~UX as required.

See the available [issue templates](.gitlab/issue_templates/) and
[merge request templates](.gitlab/issue_templates/merge_request_templates).

### Code review guidelines

You're very welcome to contribute to this project.

When contributing:

- Code: assign merge requests to a [code owner](CODEOWNERS) for review.
- Global navigation updates: assign merge requests to the
  [assigned technical writer](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments) for the
  relevant group or project for review.

For any other updates, assign to any technical writer for review.
